import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import * as actions from '../../../store/actions/dispatchActions'

import Aux from "../../../hoc/Aux";
import Modal from "../../../components/UI/Modal/Modal";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Pagination from "../../../components/UI/Pagination/Pagination";
// import {SelectPageSize} from "../../../components/UI/SearchBar/SearchBar";
import MedicalHistory from "../../../components/Lists/MedicalHistory/MedicalHistory"
import VisitDetails from '../../../components/UI/Modal/Details/VisitDetails';
import {role} from '../../../shared/utility';

class MedicalHistoryList extends Component {
    state = {
        error: null,
        showVisit: false,
        visitId: null,
        pageSize: '5'
    };

    static defaultProps = {
        defaultDoctorUrl: 'http://localhost:8081/api/doctor/patient/',
        defaultPatientUrl: 'http://localhost:8081/api/patient/medical-record/all',
    };

    componentDidMount() {
        const token = this.props.tokenRedux;
        const patientPid = this.props.match.params.pid;
        const pageSize = this.state.pageSize;
        let url = null;
        const userType = role(token);
        if (userType === 'doctor') {
            url = this.props.defaultDoctorUrl + patientPid + '/medical-record/all';
            this.props.onFetchVisits(url, token, pageSize);
        } else if (userType === 'patient') {
            url = this.props.defaultPatientUrl;
            this.props.onFetchVisits(url, token, pageSize);
        }
    }

    whatURL = pageNumber => {
        const token = this.props.tokenRedux;
        const patientPid = this.props.match.params.pid;
        const pageSize = this.state.pageSize;
        let url = null;
        const userType = role(token);
        if (userType === 'doctor') {
            url = this.props.defaultDoctorUrl + patientPid + '/medical-record/all';
            return this.props.onFetchVisitsChangePage(url, token, pageNumber, pageSize);
        } else if (userType === 'patient') {
            url = this.props.defaultPatientUrl;
            return this.props.onFetchVisitsChangePage(url, token, pageNumber, pageSize);
        }
    };

    nextPage = () => {
        const pageNumber = this.props.pageNumberRedux + 1;
        if (pageNumber < this.props.totalPagesRedux) {
            this.whatURL(pageNumber);
        }
    };

    previousPage = () => {
        const pageNumber = this.props.pageNumberRedux - 1;
        if (pageNumber >= 0) {
            this.whatURL(pageNumber);
        }
    };

    lastPage = () => {
        const pageNumber = this.props.totalPagesRedux - 1;
        if (pageNumber > this.props.pageNumberRedux) {
            this.whatURL(pageNumber);
        }
    };

    firstPage = () => {
        if (this.props.pageNumberRedux > 0) {
            this.whatURL(0);
        }
    };

    setPage = event => {
        const pageNumber = event.target.value - 1;
        if (pageNumber < this.props.totalPagesRedux && pageNumber >= 0) {
            this.whatURL(pageNumber);
        }
    };

    visitDetails = index => {
        this.setState({showVisit: !this.state.showVisit, visitId: index});
    };

    render() {
        let visitList = <Spinner/>;

        if (this.props.loadingRedux) {
            visitList = <Spinner/>;
        }
        if (this.props.errorRedux) {
            visitList = <h3>{this.props.errorRedux}</h3>
        }
        if (!this.props.tokenRedux) {
            visitList = <Redirect to='/'/>
        }
        else {
            visitList = (
                <Aux>
                    <MedicalHistory
                        visitList={this.props.visitListRedux}
                        // key={this.props.visitListRedux.date}
                        date={this.props.visitListRedux.date}
                        diagnosisId={this.props.visitListRedux.diagnosisId}
                        doctorId={this.props.visitListRedux.doctorId}
                        appointmentLength={this.props.visitListRedux.appointmentLength}
                        visitDetails={this.visitDetails}/>
                    <Pagination
                        currentPage={this.props.pageNumberRedux + 1}
                        lastPage={this.props.totalPagesRedux}
                        first={this.firstPage}
                        previous={this.previousPage}
                        next={this.nextPage}
                        last={this.lastPage}
                        setPage={(event) => this.setPage(event)}
                    />
                </Aux>
            );
        }

        let visitDetails = null;
        let visitType = null;
        if (this.state.showVisit) {
            const index = this.state.visitId;
            const visit = this.props.visitListRedux[index];
            if (visit.repeatVisitation) {
                visitType = "Pakartotinis vizitas";
            } else {
                visitType = "Pirminis vizitas";
            }
            visitDetails = <Modal show={this.state.showVisit} hide={this.visitDetails}>
                <VisitDetails
                    date={visit.date}
                    visitType={visitType}
                    diagnosisId={visit.diagnosisId}
                    doctorFirstName={visit.doctorId}
                    appointmentLength={visit.appointmentLength}
                    notes={visit.notes}
                    hide={this.visitDetails}
                    back={this.visitDetails}>
                </VisitDetails>
            </Modal>
        }
        return (
            <div className="container">
                {visitList}
                {visitDetails}
            </div>
        );
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        visitListRedux: state.fetchVisits.visitList,
        loadingRedux: state.fetchVisits.loading,
        errorRedux: state.fetchVisits.error,
        tokenRedux: state.auth.accessToken,
        authUserRedux: state.auth.authUser,
        // userPidRedux: state.auth.userPid,
        totalPagesRedux: state.fetchVisits.totalPages,
        totalElementsRedux: state.fetchVisits.totalElements,
        pageSizeRedux: state.fetchVisits.pageSize,
        pageNumberRedux: state.fetchVisits.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchVisits: (url, token, pageSize) => dispatch(actions.fetchVisits(url, token, pageSize)),
        onFetchVisitsChangePage: (url, token, pageNumber, pageSize) => dispatch(actions.changePageFetchVisits(url, token, pageNumber, pageSize))
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MedicalHistoryList));
