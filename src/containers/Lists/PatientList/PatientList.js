import React, {Component} from 'react';
import axios from "axios/index";
import {CSVLink, CSVDownload} from 'react-csv';
import {Button} from "react-bootstrap";
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions'
import {withRouter} from "react-router-dom";

import Aux from "../../../hoc/Aux";
import DoctorUserList from "../../../components/Lists/DoctorUserList/DoctorUserList";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Pagination from "../../../components/UI/Pagination/Pagination";
import {SearchFilterByFirstName, SearchFilterByLastName, SearchFilterByPid, SelectPageSize, SearchFilterByDiagnosis} from "../../../components/UI/SearchBar/SearchBar";
import {onlyNumbers, toFirstUpperCaseToLowerCase} from "../../../shared/utility";


class PatientList extends Component {

    state = {
        filtered: {
            diagnosisTitle: '',
            doctorId: 0,
            firstName: '',
            lastName: '',
            pid: 0,
        },
        pageSize: '10',
        diagnoses: [],
        showUser: false,
        userId: null,
        exportPatients: null
    };

    static defaultProps = {
        defaultUrl: 'http://localhost:8081/api/doctor/patient/search/',
        defaultSearch: {
            firstName: '',
            lastName: '',
            pid: 0,
            diagnosisTitle: '',
            doctorId: 0
        }
    };

    componentWillMount() {
        this.props.onFetchUsersInit();
        const token = this.props.tokenRedux;
        axios.get('http://localhost:8081/api/diagnoses/all', {
            headers: {'Authorization': 'Bearer ' + token},
            params: {'size': 50}})
            .then(response => {
                const spec = response.data.content;
                const onlyTitles = spec.map(spec => {return spec.title});
                console.log('fetchDiagnoses', onlyTitles);
                this.setState({diagnoses: onlyTitles})})
            .catch(() => {this.setState({error: true})})
    }

    componentDidMount() {
        console.log('DoctorPatientList didMount searchValue',this.state.searchValue);
        // isidedu ir siunciu tokena
        const token = this.props.tokenRedux;
        const url = this.props.defaultUrl;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        this.props.onFetchUsers(url, token, pageSize, searchParams);
    }

    showUserDetails = index => this.props.history.push('/doctor/patientdetails/' + this.props.patientListRedux[index].pid);

    changePage = pageNumber => {
        const token = this.props.tokenRedux;
        const url = this.props.defaultUrl;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        return this.props.onFetchUsersChangePage(url, token, pageNumber, pageSize, searchParams);
    };
    nextPage = () => {
        const pageNumber = this.props.pageNumberRedux + 1;
        if (pageNumber < this.props.totalPagesRedux) {
            this.changePage(pageNumber);
        }
    };
    previousPage = () => {
        const pageNumber = this.props.pageNumberRedux - 1;
        if (pageNumber >= 0) {
            this.changePage(pageNumber);
        }
    };
    lastPage = () => {
        const pageNumber = this.props.totalPagesRedux - 1;
        if (pageNumber > this.props.pageNumberRedux) {
            this.changePage(pageNumber);
        }
    };
    firstPage = () => {
        if (this.props.pageNumberRedux > 0) {
            this.changePage(0);
        }
    };
    setPage = event => {
        const pageNumber = event.target.value - 1;
        if (pageNumber < this.props.totalPagesRedux && pageNumber >= 0) {
            this.changePage(pageNumber);
        }
    };

    filterHandler = (event, element) => {
        const searchParams = {...this.state.filtered};
        searchParams[element] = event.target.value;
        if (!searchParams.pid) {
            searchParams.pid = 0;
            this.setState({filtered: searchParams});
        } else if (!/^\d+$/.test(searchParams.pid.toString())) {
            if (searchParams.pid.toString().length > 1) {
                const sub = searchParams.pid.toString().length - 1;
                searchParams.pid = searchParams.pid.toString().substring(0, sub);
                this.setState({filtered: searchParams});
            } else {
                searchParams.pid = 0;
                this.setState({filtered: searchParams});
            }
        } else {
            this.setState({filtered: searchParams});
        }
        const token = this.props.tokenRedux;
        const url = this.props.defaultUrl;
        const pageSize = this.state.pageSize;
        if (searchParams.pid > 3 || /^\d+$/.test(searchParams.pid.toString())) {
            if (searchParams.lastName.length >= 3 || searchParams.pid.toString().length >= 3 || searchParams.firstName.length >= 3) {
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            } else if (searchParams.lastName.length === 0 && searchParams.pid.toString() === '0' && searchParams.firstName.length === 0) {
                const searchParams = {...this.props.defaultSearch, diagnosisTitle: this.state.filtered.diagnosisTitle};
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            }
        }
    };

    pageSizeHandler = (event, index, pageSize) => {
        this.setState({pageSize: pageSize});
        const token = this.props.tokenRedux;
        const url = this.props.defaultUrl;
        if (!this.state.filtered.pid) {
            const searchParams = {...this.state.filtered, pid: 0};
            this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
        }
    };

    diagnosisHandler = event => {
        const token = this.props.tokenRedux;
        const url = this.props.defaultUrl;
        const pageSize = this.state.pageSize;
        if (this.state.diagnoses.includes(event)) {
            let searchParams = {...this.state.filtered, diagnosisTitle: event};
            this.setState({filtered: searchParams});
            if (!this.state.filtered.pid) {
                const searchParams = {...this.state.filtered, pid: 0, diagnosisTitle: event};
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            } else {
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            }
        }
        if (event.length === 0) {
            const searchParams = {...this.state.filtered, pid: 0, diagnosisTitle: ''};
            this.setState({filtered: searchParams});
            this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
        }
    };

    patientExportHandler = () => {
        const token = this.props.tokenRedux;
        axios.get('http://localhost:8081/api/doctor/patient/all', {
            headers: {'Authorization': 'Bearer ' + token},
            params: {'size': this.props.totalElementsRedux}})
            .then(response => {
                const spec = response.data.content;
                this.setState({exportPatients: spec})})
            .catch(() => {this.setState({error: true})});
    };

    render() {

        let pid = onlyNumbers(this.state.filtered.pid);
        if (this.state.filtered.pid === 0) {
            pid = '';
        }
        const firstName = toFirstUpperCaseToLowerCase(this.state.filtered.firstName);
        const lastName = toFirstUpperCaseToLowerCase(this.state.filtered.lastName);
        let patientList = <Spinner/>;

        if (this.props.loadingRedux) {
            patientList = <Spinner/>;
        }
        if (this.props.errorRedux) {
            patientList = <h3>{this.props.errorRedux}</h3>
        }

        let downloadPatientList = <Button bsStyle="success" onClick={this.patientExportHandler}>Atsisiųsti pacientų sąrašą</Button>;

        const headers = [
            {label: 'Asmens kodas', key: 'pid'},
            {label: 'Vardas', key: 'firstName'},
            {label: 'Pavardė', key: 'lastName'},
            {label: 'Gimimo data', key: 'dob'}
        ];

        if(this.state.exportPatients){
            downloadPatientList = <Aux>
                <CSVDownload
                    target="_self"
                    specs='name= "ddd"'
                    data={this.state.exportPatients}
                    headers={headers}
                    style={{color: '#fff', textDecoration: 'none'}}>
                </CSVDownload>
                <CSVLink
                    data={this.state.exportPatients}
                    filename='pacientų_sąrašas.csv' headers={headers}
                    style={{color: '#fff', textDecoration: 'none'}}>
                    <Button bsStyle="success">Atsisiųsti pacientų sąrašą</Button>
                </CSVLink>
            </Aux>
        }

        if (this.props.patientListRedux) {
            patientList = (
                <Aux>
                    <div style={{textAlign: 'left'}}>
                        <SelectPageSize
                            pageSize={this.state.pageSize}
                            handleChange={this.pageSizeHandler}/>
                    </div>
                    <Aux>
                        <SearchFilterByDiagnosis
                            diagnoses={this.state.diagnoses}
                            searchValue={this.state.filtered.diagnosisTitle}
                            handleChange={(event) => this.diagnosisHandler(event)}/>
                        <SearchFilterByPid
                            searchValue={pid}
                            handleChange={(event) => this.filterHandler(event, 'pid')}/>
                        <SearchFilterByFirstName
                            searchValue={firstName}
                            handleChange={(event) => this.filterHandler(event, 'firstName')}/>
                        <SearchFilterByLastName
                            searchValue={lastName}
                            handleChange={(event) => this.filterHandler(event, 'lastName')}/>
                    </Aux>
                    <DoctorUserList
                        userList={this.props.patientListRedux}
                        pid={this.props.patientListRedux.pid}
                        firstName={this.props.patientListRedux.firstName}
                        lastName={this.props.patientListRedux.lastName}
                        userDetails={this.showUserDetails}
                        userVisit={(index) => this.props.history.push('/doctor/newrecord/' + index)}
                        userPrescription={(index) => this.props.history.push('/doctor/newprescription/' + index)}
                    />
                    <Pagination
                        currentPage={this.props.pageNumberRedux + 1}
                        lastPage={this.props.totalPagesRedux}
                        first={this.firstPage}
                        previous={this.previousPage}
                        next={this.nextPage}
                        last={this.lastPage}
                        setPage={(event) => this.setPage(event)}
                    />
                </Aux>);
        }
        return (
            <div className="container">
                {downloadPatientList}
                {patientList}
            </div>
        )
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        patientListRedux: state.fetch.userList,
        loadingRedux: state.fetch.loading,
        errorRedux: state.fetch.error,
        // pasiimu tokena
        tokenRedux: state.auth.accessToken,
        userPidRedux: state.auth.userPid,
        totalPagesRedux: state.fetch.totalPages,
        totalElementsRedux: state.fetch.totalElements,
        pageSizeRedux: state.fetch.pageSize,
        pageNumberRedux: state.fetch.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchUsersInit: () => dispatch(actions.fetchUsersInit()),
        onFetchUsers: (url, token, pageNumber, pageSize, searchParams) => dispatch(actions.fetchUsers(url, token, pageNumber, pageSize, searchParams)),
        onFetchUsersChangePage: (url, token, pageNumber, pageSize, searchParams) => dispatch(actions.changePageFetchUsers(url, token, pageNumber, pageSize, searchParams)),
    };
};

// errorHandler wraps PatientListClass
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PatientList));
