import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import * as actions from '../../../store/actions/dispatchActions';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import Aux from "../../../hoc/Aux";
import Modal from "../../../components/UI/Modal/Modal";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Pagination from "../../../components/UI/Pagination/Pagination";
// import { SelectPageSize } from "../../../components/UI/SearchBar/SearchBar";
import PrescriptionListComp from "../../../components/Lists/Prescription/PrescriptionListComp"
import PrescriptionDetails from '../../../components/UI/Modal/Details/PrescriptionDetails';
import {role} from "../../../shared/utility";


class PrescriptionList extends Component {
    state = {
        error: null,
        showPrescription: false,
        prescriptionId: null,
        pageSize: '5'
    };

    static defaultProps = {
        defaultPatientUrl: 'http://localhost:8081/api/patient/prescription/all',
        defaultDoctorUrl: 'http://localhost:8081/api/doctor/patient/'
    };

    componentDidMount() {
        const pageSize = this.state.pageSize;
        const patientPid = this.props.match.params.pid;
        const token = this.props.tokenRedux;
        const userType = role(token);
        let url = null;
        if (userType === 'doctor') {
            url = this.props.defaultDoctorUrl + patientPid + '/prescription/all';
            return this.props.onFetchPrescriptions(url, token, pageSize);
        } else if (userType === 'patient') {
            url = this.props.defaultPatientUrl;
            return this.props.onFetchPrescriptions(url, token, pageSize);
        }
    }

    whatURL = pageNumber => {
        const pageSize = this.state.pageSize;
        const patientPid = this.props.match.params.pid;
        const token = this.props.tokenRedux;
        const userType = role(token);
        let url = null;
        if (userType === 'doctor') {
            url = this.props.defaultDoctorUrl + patientPid + '/prescription/all';
            return this.props.onFetchPrescriptionsChangePage(url, token, pageNumber, pageSize);
        } else if (userType === 'patient') {
            url = this.props.defaultPatientUrl;
            return this.props.onFetchPrescriptionsChangePage(url, token, pageNumber, pageSize);
        }
    };

    nextPage = () => {
        const pageNumber = this.props.pageNumberRedux + 1;
        if (pageNumber < this.props.totalPagesRedux) {
            this.whatURL(pageNumber);
        }
    };

    previousPage = () => {
        const pageNumber = this.props.pageNumberRedux - 1;
        if(pageNumber >= 0){
            this.whatURL(pageNumber);
        }
    };

    lastPage = () => {
        const pageNumber = this.props.totalPagesRedux - 1;
        if (pageNumber > this.props.pageNumberRedux){
            this.whatURL(pageNumber);
        }
    };

    firstPage = () => {
        if (this.props.pageNumberRedux > 0){
            this.whatURL(0)
        }
    };

    setPage = event => {
        const pageNumber = event.target.value - 1;
        if (pageNumber < this.props.totalPagesRedux && pageNumber >= 0){
            this.whatURL(pageNumber);
        }
    };

    prescriptionDetails = index => {
        this.setState({ showPrescription: !this.state.showPrescription, prescriptionId: index });
    };

    render() {
        let prescriptionList = <Spinner />;

        if (this.props.loadingRedux) {
            prescriptionList = <Spinner />;
        }
        if (this.props.errorRedux) {
            prescriptionList = <h3>{this.props.errorRedux}</h3>
        }
        if (!this.props.tokenRedux) {
            prescriptionList = <Redirect to='/' />
        }
        else {
            // if (this.props.prescriptionListRedux.validUntil === null) {
            //     validUntil = "Neterminuotas";
            //     console.log("neterminuotas", this.props.prescriptionListRedux.validUntil);
            // } else {
            //     validUntil = this.props.prescriptionListRedux.validUntil;
            //     console.log("paprastas",lastPageprescriptionListRedux.validUntil);
            // }
            prescriptionList = (
                <Aux>
                    <PrescriptionListComp
                        prescriptionList={this.props.prescriptionListRedux}
                        key={this.props.prescriptionListRedux.prescriptionId}
                        validUntil={this.props.prescriptionListRedux.validUntil}
                        prescribedOn={this.props.prescriptionListRedux.prescriptionDate}
                        activeIngredient={this.props.prescriptionListRedux.activeIngredient}
                        prescriptionAction={this.prescriptionDetails}
                        buttonTitle="Detaliau" />
                    <Pagination
                        currentPage={this.props.pageNumberRedux + 1}
                        lastPage={this.props.totalPagesRedux}
                        first={this.firstPage}
                        previous={this.previousPage}
                        next={this.nextPage}
                        last={this.lastPage}
                        setPage={(event) => this.setPage(event)}
                    />
                </Aux>
            );
        }

        let prescriptionDetails = null;
        let validUntil = null;
        if (this.state.showPrescription) {
            const index = this.state.prescriptionId;
            const prescription = this.props.prescriptionListRedux[index];
            if (!prescription.validUntil) {
                validUntil = "Neterminuotas";
                console.log("neterminuotas", prescription.validUntil);
            } else {
                validUntil = prescription.validUntil;
                console.log("paprastas", prescription.validUntil);
            }
            prescriptionDetails = <Modal show={this.state.showPrescription} hide={this.prescriptionDetails}>
                <PrescriptionDetails
                    prescribedOn={prescription.prescriptionDate}
                    validUntil={validUntil}
                    activeIngredient={prescription.activeIngredient}
                    activeIngredientPerDose={prescription.activeIngredientPerDose}
                    activeIngredientUnits={prescription.activeIngredientUnits}
                    dosageNotes={prescription.dosageNotes}
                    hide={this.prescriptionDetails}
                    back={this.prescriptionDetails}>
                </PrescriptionDetails>
            </Modal>
        }
        return (
            <div className="container">
                {prescriptionList}
                {prescriptionDetails}
            </div>
        );
    }
}
// Redux states
const mapStateToProps = state => {
    return {
        prescriptionListRedux: state.fetchPrescriptions.prescriptionList,
        loadingRedux: state.fetchPrescriptions.loading,
        errorRedux: state.fetchPrescriptions.error,
        tokenRedux: state.auth.accessToken,
        userPidRedux: state.auth.userPid,
        totalPagesRedux: state.fetchPrescriptions.totalPages,
        totalElementsRedux: state.fetchPrescriptions.totalElements,
        pageSizeRedux: state.fetchPrescriptions.pageSize,
        pageNumberRedux: state.fetchPrescriptions.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchPrescriptions: (url, token, pageSize) => dispatch(actions.fetchPrescriptions(url, token, pageSize)),
        onFetchPrescriptionsChangePage: (url, token, pageNumber, pageSize) => dispatch(actions.changePageFetchPrescriptions(url, token, pageNumber, pageSize))

    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrescriptionList));
