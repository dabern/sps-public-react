import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from "react-redux";
import {Redirect} from "react-router-dom";

import Aux from "../../../hoc/Aux";
import Spinner from "../../../components/UI/Spinner/Spinner";
import UserCard from '../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../assets/usericon-guy.png';

import UserChangePassword from "../../Administration/UserAccount/UserChangePassword";

class PatientProfile extends Component {
    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: 0,
            dob: ''
        },
        showForm: false
    };

    componentDidMount() {
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            const authUser = this.props.userDetailsRedux;
            const updatedUser = { ...authUser, image: usericon, loading: false };
            this.setState({ user: updatedUser })
        }
    }

    changePasswordHandler = () => this.setState({ showForm: !this.state.showForm });

    render() {
        let changePasswordForm = null;
        if (this.state.showForm) {
            changePasswordForm = <UserChangePassword back={this.changePasswordHandler} />
        }
        let userCard = null;
        if (!this.props.userDetailsRedux) {
            userCard = <Redirect to='/patient/home' />
        } else if (this.state.loading || this.props.loadingRedux) {
            userCard = <Spinner />
        } else if (this.state.error || this.props.errorRedux) {
            userCard = <h3>Tinklo klaida</h3>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                Gimimo data: {this.state.user.dob}
                <p>
                    <Button id='patientProfileChangePassword' bsStyle="info" onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(PatientProfile);
