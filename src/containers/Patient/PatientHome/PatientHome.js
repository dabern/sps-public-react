import React, { Component } from 'react';
import Aux from "../../../hoc/Aux";

import MedicalHistoryList from '../../Lists/MedicalHistoryList/MedicalHistoryList';
import PrescriptionList from '../../Lists/PrescriptionList/PrescriptionList'

class PatientHome extends Component { 
    render() {
        return (
            <Aux>
                <h3>Ligos istorija</h3>
                <br/>
                <MedicalHistoryList/>
                <br/>
                <h3>Išrašyti receptai</h3>
                <br/>
                <PrescriptionList/>
            </Aux>
        );
    }
}
export default PatientHome;