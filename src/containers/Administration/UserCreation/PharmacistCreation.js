import React, {Component} from 'react';
import PharmacistForm from '../../../components/Form/Forms/PharmacistForm';
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions';
import {registerField} from "redux-form";
import Modal from "../../../components/UI/Modal/Modal";
import CreatedCard from "../../../components/UI/Modal/CreatedUser/CreatedUser";
import icon from '../../../assets/usericon-guy.png';

class PharmacistCreation extends Component {

    state = {
        isCreated: false,
        image: icon
    };

    componentWillMount() {
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        this.props.onInitCreation();
        this.props.regFieldPid();
        this.props.regFieldFirstName();
        this.props.regFieldLastName();
        this.props.regFieldOrganization();
        this.props.regFieldType();
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined') {
            const url = 'http://localhost:8081/api/admin/pharmacist/' + number;
            this.props.onFetchUser(url, token);
        }
    }

    pharmacistCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/admin/new/pharmacist/';
        const number = this.props.match.params.id;
        const token = this.props.tokenRedux;
        const formName = 'PharmacistForm';
        const type = 'Vaistininkas';
        const data = {...values, user: 'pharmacist'};
        const accountData = {username: values.username, password: values.password};
        if (typeof number !== 'undefined') {
            url = 'http://localhost:8081/api/admin/update/pharmacist/' + number;
            this.props.onUpdateUser(url, data, token, type);
        } else {
            this.props.onCreateUser(url, data, token, formName, type, accountData);
        }
    };

    hideModal = () => {
        this.setState({isCreated: false})
    };

    editCreatedUser = () => {
        this.props.history.replace('/admin/register/pharmacist/' + this.props.createdUserDataRedux.id);
        this.setState({isCreated: false});
    };

    render() {

        console.log('PharmacistCreation render');
        const create = typeof this.props.match.params.id === 'undefined';

        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            const workplace = this.props.createdUserDataRedux.type + ' ' + this.props.createdUserDataRedux.organization;
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedCard
                    image={this.state.image}
                    firstName={this.props.createdUserDataRedux.firstName}
                    lastName={this.props.createdUserDataRedux.lastName}
                    pid={this.props.createdUserDataRedux.pid}
                    hide={this.hideModal}
                    back={() => this.props.history.push('/admin/userList')}
                    edit={this.editCreatedUser}
                    created={create}>
                    <span>Darbovietė: <strong>{workplace}</strong></span>
                </CreatedCard>
            </Modal>
        }
        return (
            <div>
                {createdUser}
                <PharmacistForm created={create} onSubmit={this.pharmacistCreationHandler}
                                back={this.props.history.goBack}>
                    {errorMessage}
                </PharmacistForm>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onUpdateUser: (url, userData, token, type) => dispatch(actions.userUpdate(url, userData, token, type)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        regFieldPid: () => dispatch(registerField('PharmacistForm', 'pid', 'Field')),
        regFieldFirstName: () => dispatch(registerField('PharmacistForm', 'firstName', 'Field')),
        regFieldLastName: () => dispatch(registerField('PharmacistForm', 'lastName', 'Field')),
        regFieldOrganization: () => dispatch(registerField('PharmacistForm', 'organization', 'Field')),
        regFieldType: () => dispatch(registerField('PharmacistForm', 'type', 'Field')),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PharmacistCreation);
