import React, {Component} from 'react';
import PatientForm from '../../../components/Form/Forms/PatientForm';
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions';
import Modal from "../../../components/UI/Modal/Modal";
import {registerField} from "redux-form";
import CreatedCard from "../../../components/UI/Modal/CreatedUser/CreatedUser";
import icon from '../../../assets/usericon.png';

class PatientCreation extends Component {

    state = {
        isCreated: false,
        image: icon
    };

    componentWillMount() {
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        this.props.onInitCreation();
        this.props.regFieldPid();
        this.props.regFieldFirstName();
        this.props.regFieldLastName();
        this.props.regFieldDob();
        // this.props.regFieldDoctorPid();
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined') {
            const url = 'http://localhost:8081/api/admin/patient/' + number;
            this.props.onFetchUser(url, token);
        }
    }

    patientCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/admin/new/patient/';
        const number = this.props.match.params.id;
        const token = this.props.tokenRedux;
        const formName = 'PatientForm';
        const type = 'Pacientas';
        const data = {...values, user: 'patient', doctorPid: values.doctorPidNullable};
        const accountData = {username: 'default', password: 'default'};
        if (typeof number !== 'undefined') {
            url = 'http://localhost:8081/api/admin/update/patient/' + number;
            this.props.onUpdateUser(url, data, token, type);
        } else {
            this.props.onCreateUser(url, data, token, formName, type, accountData);
        }
    };

    hideModal = () => {
        this.setState({isCreated: false})
    };

    editCreatedUser = () => {
        this.props.history.replace('/admin/register/patient/' + this.props.createdUserDataRedux.id);
        this.setState({isCreated: false});
    };

    render() {

        console.log('PatientCreation render');
        const create = typeof this.props.match.params.id === 'undefined';

        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            const dob = new Date(this.props.createdUserDataRedux.dob).toLocaleDateString("lt-LT");
            const doctorPid = this.props.createdUserDataRedux.doctorPid;
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedCard
                    image={this.state.image}
                    firstName={this.props.createdUserDataRedux.firstName}
                    lastName={this.props.createdUserDataRedux.lastName}
                    pid={this.props.createdUserDataRedux.pid}
                    hide={this.hideModal}
                    back={() => this.props.history.push('/admin/userList')}
                    edit={this.editCreatedUser}
                    created={create}>
                    <p>Gimimo data: <strong>{dob}</strong></p>
                    <p>Gydytojo asmens kodas: <strong>{doctorPid}</strong></p>
                </CreatedCard>
            </Modal>
        }
        return (
            <div>
                {createdUser}
                <PatientForm created={create} onSubmit={this.patientCreationHandler} back={this.props.history.goBack}>
                    {errorMessage}
                </PatientForm>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onUpdateUser: (url, userData, token, type) => dispatch(actions.userUpdate(url, userData, token, type)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        regFieldPid: () => dispatch(registerField('PatientForm', 'pid', 'Field')),
        regFieldFirstName: () => dispatch(registerField('PatientForm', 'firstName', 'Field')),
        regFieldLastName: () => dispatch(registerField('PatientForm', 'lastName', 'Field')),
        regFieldDob: () => dispatch(registerField('PatientForm', 'dob', 'Field')),
        // regFieldDoctorPid: () => dispatch(registerField('PatientForm', 'doctorPid', 'Field'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PatientCreation);
