import React, {Component} from 'react';
import AdminForm from '../../../components/Form/Forms/AdminForm'
import {connect} from "react-redux";
import {registerField} from 'redux-form'
import * as actions from '../../../store/actions/dispatchActions'
import Modal from "../../../components/UI/Modal/Modal";
import CreatedCard from "../../../components/UI/Modal/CreatedUser/CreatedUser";
import icon from '../../../assets/usericon-guy.png';

class AdminCreation extends Component {

    state = {
        isCreated: false,
        image: icon
    };

    componentWillMount() {
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        this.props.onInitCreation();
        this.props.regFieldPid();
        this.props.regFieldFirstName();
        this.props.regFieldLastName();
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined') {
            const url = 'http://localhost:8081/api/admin/admin/' + number;
            this.props.onFetchUser(url, token);
        }
    }

    adminCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/admin/new/admin/';
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const formName = 'AdminForm';
        const type = 'Administratorius';
        const data = {...values, user:'admin'};
        const accountData = {username: values.username, password: values.password};
        if (typeof number !== 'undefined') {
            url = 'http://localhost:8081/api/admin/update/admin/' + number;
            this.props.onUpdateUser(url, data, token, type);
        } else {
            this.props.onCreateUser(url, data, token, formName, type, accountData);
        }
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    editCreatedUser = () => {
        this.props.history.replace('/admin/register/admin/' + this.props.createdUserDataRedux.id);
        this.setState({isCreated: false});
    };

    render() {

        console.log('AccountCreation render');

        const create = typeof this.props.match.params.id === 'undefined';
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedCard
                    image={this.state.image}
                    firstName={this.props.createdUserDataRedux.firstName}
                    lastName={this.props.createdUserDataRedux.lastName}
                    pid={this.props.createdUserDataRedux.pid}
                    hide={this.hideModal}
                    back={() => this.props.history.push('/admin/userList')}
                    edit={this.editCreatedUser}
                    created={create}>
                    {/*<span>Vartotojo vardas: <strong>*/}
                    {/*{this.props.createdUserDataRedux.username}*/}
                    {/*</strong></span>*/}
                </CreatedCard>
            </Modal>
        }
        return (
            <div>
                {createdUser}
                <AdminForm created={create} onSubmit={this.adminCreationHandler} back={this.props.history.goBack}>
                {errorMessage}
                </AdminForm>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onUpdateUser: (url, userData, token, type) => dispatch(actions.userUpdate(url, userData, token, type)),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        regFieldPid: () => dispatch(registerField('AdminForm', 'pid', 'Field')),
        regFieldFirstName: () => dispatch(registerField('AdminForm', 'firstName', 'Field')),
        regFieldLastName: () => dispatch(registerField('AdminForm', 'lastName', 'Field'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminCreation);
