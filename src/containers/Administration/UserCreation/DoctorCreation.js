import React, {PureComponent} from 'react';
import DoctorForm from '../../../components/Form/Forms/DoctorForm';
import SpecializationForm from '../../../components/Form/Forms/SpecializationForm'
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions';
import {reset} from "redux-form";
import Modal from "../../../components/UI/Modal/Modal";
import CreatedCard from "../../../components/UI/Modal/CreatedUser/CreatedUser";
import icon from '../../../assets/usericon.png';
import axios from 'axios';
import Spinner from "../../../components/UI/Spinner/Spinner";

class DoctorCreation extends PureComponent {

    state = {
        isCreated: false,
        image: icon,
        specializations: null,
        error: false
    };

    componentWillMount() {
        this.props.onFetchUserInit();
        const token = this.props.tokenRedux;
        axios.get('http://localhost:8081/api/specializations/all', {
            headers: {'Authorization': 'Bearer ' + token},
            params: {'size': 500}})
            .then(response => {
                const spec = response.data.content;
                const onlyTitlesAndId = spec.map(spec => {
                    return {id: spec.id, title: spec.title}});
                this.setState({specializations: onlyTitlesAndId})})
            .catch(() => {
                this.setState({error: true})
             })
    }

    componentDidMount() {
        const token = this.props.tokenRedux;
        this.props.onInitCreation();
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined') {
            const url = 'http://localhost:8081/api/admin/doctor/' + number;
            this.props.onFetchUser(url, token);
        }
    }

    doctorCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/admin/new/doctor/';
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const formName = 'DoctorForm';
        const type = 'Gydytojas';
        const data = {...values, user: 'doctor'};
        const accountData = {username: values.username, password: values.password};
        if (typeof number !== 'undefined') {
            url = 'http://localhost:8081/api/admin/update/doctor/' + number;
            this.props.onUpdateUser(url, data, token, type);
        } else {
            this.props.onCreateUser(url, data, token, formName, type, accountData)
        }
    };

    hideModal = () => {
        this.setState({isCreated: false})
    };

    editCreatedUser = () => {
        this.props.history.replace('/admin/register/doctor/' + this.props.createdUserDataRedux.id);
        this.setState({isCreated: false});
    };

    specializationCreationHandler = values => {
        this.setState({isCreated: false});
        const url = 'http://localhost:8081/api/specializations/new';
        const token = this.props.tokenRedux;
        // this.props.afterSubmit();
        // this.props.history.push(this.props.match.url);
        const data = {...values, entityType: 'specialization'};
        this.props.onCreateSpecialization(url, data, token);
        this.componentWillMount();
        // return <Redirect to={this.props.match.url}/>
    };

    render() {

        // const red = <Redirect to={this.props.match.url}/>;
        let createSpecialization = null;
        let errorSpec = null;
        if(this.props.errorSpecRedux){
            errorSpec = <span>{this.props.errorSpecRedux}</span>
        }
        if (typeof this.props.formValues.DoctorForm !== 'undefined') {
            if (typeof this.props.formValues.DoctorForm.values !== 'undefined') {
                const values = this.props.formValues.DoctorForm.values;
                if (typeof values.specializationId !== 'undefined') {
                    if (values.specializationId === 0) {
                        createSpecialization = <Modal show={values.specializationId === 0}>
                            <SpecializationForm onSubmit={this.specializationCreationHandler}
                                                back={this.props.afterSubmit}>
                                {errorSpec}
                            </SpecializationForm>
                        </Modal>
                    }
                }
            }
        }

        console.log('DoctorCreation render', this.state.specializations);
        const create = typeof this.props.match.params.id === 'undefined';

        let localError = null;
        if (this.state.error) {
            localError = <h2>Tinklo klaida</h2>
        }
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        let form = null;
        if(this.props.loadingRedux || this.props.loadingSpecRedux){
            form = <Spinner/>
        }
         if (this.state.specializations) {
            form = <DoctorForm
                spec={this.state.specializations}
                created={create}
                showButtons={!createSpecialization}
                onSubmit={this.doctorCreationHandler}
                back={this.props.history.goBack}>
                {errorMessage}
            </DoctorForm>;
            if (this.props.createdUserDataRedux && this.props.createdUserDataRedux.specializationId && this.props.createdRedux) {
                const doctorSpec = this.props.createdUserDataRedux.specializationId - 1;
                createdUser = <Modal
                    show={this.state.isCreated}
                    hide={this.hideModal}>
                    <CreatedCard
                        image={this.state.image}
                        firstName={this.props.createdUserDataRedux.firstName}
                        lastName={this.props.createdUserDataRedux.lastName}
                        pid={this.props.createdUserDataRedux.pid}
                        hide={this.hideModal}
                        back={() => this.props.history.push('/admin/userList')}
                        edit={this.editCreatedUser}
                        created={create}>
                        <span>Specializacija: <strong>{this.state.specializations[doctorSpec].title}</strong></span>
                    </CreatedCard>
                </Modal>
            }
        }

        return (
            <div>
                {createdUser}
                {form}
                {createSpecialization}
                {localError}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        formValues: state.form,
        loadingSpecRedux: state.specialization.loading,
        errorSpecRedux: state.specialization.errorSpec
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onUpdateUser: (url, userData, token, type) => dispatch(actions.userUpdate(url, userData, token, type)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        afterSubmit: () => dispatch(reset('DoctorForm')),
        onCreateSpecialization: (url, userData, token) => dispatch(actions.specializationCreation(url, userData, token))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DoctorCreation);
