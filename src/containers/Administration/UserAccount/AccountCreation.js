import React, {Component} from 'react';
import AccountForm from '../../../components/Form/Forms/AccountForm'
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions'
import Modal from "../../../components/UI/Modal/Modal";
import CreatedAccount from "../../../components/UI/Modal/CreatedAccount/CreatedAccount";
import icon from '../../../assets/usericon-guy.png';

class AccountCreation extends Component {

    state = {
        isCreated: false,
        image: icon
    };

    componentWillMount() {
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const url = 'http://localhost:8081/api/admin/patient/' + number;
        this.props.onFetchUser(url, token);
    }

    adminCreationHandler = values => {
        this.setState({isCreated: true});
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const url = 'http://localhost:8081/api/admin/new/patient-account/' + number;
        const form = 'AccountForm';
        const type = 'Vartotojas';
        const accountData = {username: values.username, password: values.password};
        if (typeof number !== 'undefined') {
            this.props.onCreateUser(url, accountData, token, form, type, null);
        }
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    render() {

        console.log('AccountCreation render');

        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux && this.props.userDataRedux) {
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedAccount
                    image={this.state.image}
                    firstName={this.props.userDataRedux.firstName}
                    lastName={this.props.userDataRedux.lastName}
                    pid={this.props.userDataRedux.pid}
                    hide={() => this.hideModal}
                    back={() => this.props.history.replace('/admin/userList')}
                    username={this.props.createdUserDataRedux.username}/>
            </Modal>
        }
        return (
            <div>
                {createdUser}
                <AccountForm created={true} onSubmit={this.adminCreationHandler} back={this.props.history.goBack}>
                {errorMessage}
                </AccountForm>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        // loadingRedux: state.create.loading,
        createdUserDataRedux: state.create.userData,
        // loadingRedux: state.getUser.loading,
        userDataRedux: state.getUser.userById,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountCreation);
