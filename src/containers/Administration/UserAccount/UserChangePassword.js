import React, {Component} from 'react';
import PasswordChangeForm from "../../../components/Form/Forms/PasswordChangeForm";
import axios from "axios/index";
import {connect} from "react-redux";
import {reset} from 'redux-form';
import Modal from "../../../components/UI/Modal/Modal";
import ChangedPassword from '../../../components/UI/Modal/ChangedPassword/ChangedPassword';

class UserChangePassword extends Component {
    // Container that handles the POST request to change password
    state = {
        error: false,
        showModal: false,
        changed: false
    };

    changePasswordHandler = values => {
        // Without username at the moment, will be changed later
        const token = this.props.tokenRedux;
        const currentPassword = values.password;
        const data = {username: values.user, password: values.newPasswordConfirmation};
        console.log('currentPassword', currentPassword, 'newData', data);
        axios.put("http://localhost:8081/api/me/account/update", data, {headers: {'Authorization': 'Bearer ' + token}, params: {currentPasswordGiven: currentPassword}})
            .then(response => {
                console.log('changePasswordHandler response ', response.data);
                this.props.onSubmitSuccess();
                this.setState({showModal: true, changed: true});
            }).catch(error => {
            this.setState({error: true});
            console.log('changePasswordHandler response ', error)
        });
    };

    modalHandler = () => {
        this.setState({showModal: !this.state.showModal});
    };

    render() {
        console.log('userChangePassword render');

        const successModal = <Modal show={this.state.showModal} hide={this.modalHandler}>
            <ChangedPassword hide={this.modalHandler}/></Modal>;
        let errorMessage = null;
        if (this.state.error) {
            errorMessage = <span>Neteisingi duomenys</span>;
        }

        return (
            <div>
                <PasswordChangeForm onSubmit={this.changePasswordHandler} back={this.props.back}>
                    {errorMessage}
                </PasswordChangeForm>
                {successModal}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        authUserDataRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onSubmitSuccess: () => dispatch(reset('PasswordChangeForm'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserChangePassword);
