import React, {PureComponent} from 'react';
import './PatientListClass.css'
import UserList from "../../components/Lists/UserList/userList";
import Spinner from "../../components/UI/Spinner/Spinner";
import {connect} from "react-redux";
import * as actions from '../../store/actions/dispatchActions'
import Aux from "../../hoc/Aux";
import Pagination from "../../components/UI/Pagination/Pagination";
import {SelectType, SearchFilterByFirstName, SearchFilterByLastName, SearchFilterByPid, SelectPageSize} from "../../components/UI/SearchBar/SearchBar";
import {Redirect} from "react-router-dom";
// import Modal from "../../components/UI/Modal/Modal";
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../shared/utility';
// import UserDetails from "../../components/UI/Modal/Details/UserDetails";
// import usericon from '../../assets/usericon-guy.png';

class AdminUserList extends PureComponent {

    state = {
        userType: 'http://localhost:8081/api/admin/admin/search',
        filtered: {
            firstName: '',
            lastName: '',
            pid: 0,
        },
        pageSize: '5',
        // showUser: false,
        // userId: null,
        // image: usericon
    };

    // example: default properties
    static defaultProps = {
        urlAdmin: '/admin/register/admin/',
        urlDoctor: '/admin/register/doctor/',
        urlPatient: '/admin/register/patient/',
        urlPharmacist: '/admin/register/pharmacist/',
        defaultSearch: {
            firstName: '',
            lastName: '',
            pid: 0,
        }
    };

    componentWillMount() {
        const token = this.props.tokenRedux;
        let url = this.state.userType;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        this.props.onFetchUsers(url, token, pageSize, searchParams);
    }

    editUser = (index, userType) => {
        userType = this.state.userType;
        switch (userType) {
            case 'http://localhost:8081/api/admin/admin/search':
                return this.props.history.push(this.props.urlAdmin + this.props.patientListRedux[index].id);
            case 'http://localhost:8081/api/admin/doctor/search':
                return this.props.history.push(this.props.urlDoctor + this.props.patientListRedux[index].id);
            case 'http://localhost:8081/api/admin/patient/search':
                return this.props.history.push(this.props.urlPatient + this.props.patientListRedux[index].id);
            case 'http://localhost:8081/api/admin/pharmacist/search':
                return this.props.history.push(this.props.urlPharmacist + this.props.patientListRedux[index].id);
            default:
                return this.props.history.push('/');
        }
    };

    // userDetails = index => {
    //     this.setState({showUser: !this.state.showUser, userId: index});
    // };

    userAccount = index => {
        this.props.history.push('/admin/register/patient/account/' + this.props.patientListRedux[index].id)
    };

    changePage = pageNumber => {
        const token = this.props.tokenRedux;
        const url = this.state.userType;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        return this.props.onFetchUsersChangePage(url, token, pageNumber, pageSize, searchParams);
    };
    nextPage = () => {
        const pageNumber = this.props.pageNumberRedux + 1;
        if (pageNumber < this.props.totalPagesRedux) {
            this.changePage(pageNumber);
        }
    };
    previousPage = () => {
        const pageNumber = this.props.pageNumberRedux - 1;
        if (pageNumber >= 0) {
            this.changePage(pageNumber);
        }
    };
    lastPage = () => {
        const pageNumber = this.props.totalPagesRedux - 1;
        if (pageNumber > this.props.pageNumberRedux) {
            this.changePage(pageNumber);
        }
    };
    firstPage = () => {
        if (this.props.pageNumberRedux > 0) {
            this.changePage(0);
        }
    };
    setPage = event => {
        const pageNumber = event.target.value - 1;
        if (pageNumber < this.props.totalPagesRedux && pageNumber >= 0) {
            this.changePage(pageNumber);
        }
    };

    userTypeHandler = (event, index, url) => {
        this.setState({userType: url});
        const token = this.props.tokenRedux;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        if (searchParams.lastName.length < 3 && searchParams.pid.toString().length < 3 && searchParams.firstName.length < 3) {
            const searchParams = this.props.defaultSearch;
            this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
        } else {
            this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
        }
    };

    filterHandler = (event, element) => {
        const searchParams = {...this.state.filtered};
        searchParams[element] = event.target.value;
        if (!searchParams.pid) {
            searchParams.pid = 0;
            this.setState({filtered: searchParams});
        } else if (!/^\d+$/.test(searchParams.pid.toString())) {
            if (searchParams.pid.toString().length > 1) {
                const sub = searchParams.pid.toString().length - 1;
                searchParams.pid = searchParams.pid.toString().substring(0, sub);
                this.setState({filtered: searchParams});
            } else {
                searchParams.pid = 0;
                this.setState({filtered: searchParams});
            }
        } else {
            this.setState({filtered: searchParams});
        }
        const token = this.props.tokenRedux;
        const url = this.state.userType;
        const pageSize = this.state.pageSize;
        if (searchParams.pid === 0 || /^\d+$/.test(searchParams.pid.toString())) {
            if (searchParams.lastName.length >= 3 || searchParams.pid.toString().length >= 3 || searchParams.firstName.length >= 3) {
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            } else if (searchParams.lastName.length === 0 && searchParams.pid.toString() === '0' && searchParams.firstName.length === 0) {
                const searchParams = this.props.defaultSearch;
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            }
        }
    };

    pageSizeHandler = (event, index, pageSize) => {
        this.setState({pageSize: pageSize});
        const token = this.props.tokenRedux;
        let url = this.state.userType;
        let searchParams = this.state.filtered;
        this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
    };

    render() {

        let patientList = <Spinner/>;

        if (this.props.loadingRedux) {
            patientList = <Spinner/>;
        }
        if (this.props.errorRedux) {
            patientList = <h3>{this.props.errorRedux}</h3>
        }
        if (!this.props.tokenRedux) {
            patientList = <Redirect to='/'/>
        }
        if (this.props.patientListRedux) {
            let pid = onlyNumbers(this.state.filtered.pid);
            if (this.state.filtered.pid === 0) {
                pid = '';
            }
            const firstName = toFirstUpperCaseToLowerCase(this.state.filtered.firstName);
            const lastName = toFirstUpperCaseToLowerCase(this.state.filtered.lastName);
            const isPatient = this.state.userType === 'http://localhost:8081/api/admin/patient/search';

            patientList = (
                <Aux>
                    <div id='AdminUserListSelectFilters' style={{textAlign: 'left'}}>
                        <SelectType
                            handleChange={this.userTypeHandler}
                            userType={this.state.userType}/>
                        <SelectPageSize
                            pageSize={this.state.pageSize}
                            handleChange={this.pageSizeHandler}/><br/>
                    </div>
                    <Aux>
                        <SearchFilterByPid
                            searchValue={pid}
                            handleChange={(event) => this.filterHandler(event, 'pid')}/>
                        <SearchFilterByFirstName
                            searchValue={firstName}
                            handleChange={(event) => this.filterHandler(event, 'firstName')}/>
                        <SearchFilterByLastName
                            searchValue={lastName}
                            handleChange={(event) => this.filterHandler(event, 'lastName')}/>
                    </Aux>
                    <UserList
                        // key={this.props.patientListRedux.pid}
                        userList={this.props.patientListRedux}
                        pid={this.props.patientListRedux.pid}
                        firstName={this.props.patientListRedux.firstName}
                        lastName={this.props.patientListRedux.lastName}
                        userEdit={this.editUser}
                        // userDetails={this.userDetails}
                        userAccount={this.userAccount}
                        patient={isPatient}
                        hasAccount={this.props.patientListRedux.hasAccount}
                    />
                    <Pagination
                        currentPage={this.props.pageNumberRedux + 1}
                        lastPage={this.props.totalPagesRedux}
                        first={this.firstPage}
                        previous={this.previousPage}
                        next={this.nextPage}
                        last={this.lastPage}
                        setPage={(event) => this.setPage(event)}
                    />
                </Aux>);
        }

        return (
            <div className="container">
                {patientList}
            </div>
        )
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        patientListRedux: state.fetch.userList,
        loadingRedux: state.fetch.loading,
        errorRedux: state.fetch.error,
        tokenRedux: state.auth.accessToken,
        totalPagesRedux: state.fetch.totalPages,
        totalElementsRedux: state.fetch.totalElements,
        pageSizeRedux: state.fetch.pageSize,
        pageNumberRedux: state.fetch.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchUsers: (url, token, pageSize, data) => dispatch(actions.fetchUsers(url, token, pageSize, data)),
        onFetchUsersChangePage: (url, token, pageNumber, pageSize, searchParams) => dispatch(actions.changePageFetchUsers(url, token, pageNumber, pageSize, searchParams)),
    };
};

// errorHandler wraps PatientListClass
export default connect(mapStateToProps, mapDispatchToProps)(AdminUserList);
