import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import {connect} from "react-redux";
import UserCard from '../../components/DetailCards/UserCard/UserCard';
import usericon from '../../assets/usericon-guy.png';
import Aux from "../../hoc/Aux";
import UserChangePassword from "./UserAccount/UserChangePassword";
import Spinner from "../../components/UI/Spinner/Spinner";

class AdminProfile extends Component {

    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: ''
        },
        showForm: false,
        error: false
    };

    componentDidMount() {
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            const authUser = this.props.userDetailsRedux;
                const updatedUser = {...authUser, image: usericon, loading: false};
                this.setState({user: updatedUser})
        }
    }

    changePasswordHandler = () => this.setState({showForm: !this.state.showForm});

    render() {
        console.log('AdminProfile render');

        let changePasswordForm = null;
        if (this.state.showForm) {
            changePasswordForm = <UserChangePassword back={this.changePasswordHandler}/>
        }
        let userCard = null;
        if(this.state.loading){
            userCard = <Spinner/>
        } else if (this.state.error){
            userCard = <h2>Error</h2>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                <p>
                    <Button id='adminChangePasswordButton' bsStyle="info" onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(AdminProfile);
