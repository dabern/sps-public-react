import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import axios from 'axios';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import * as actions from '../../../store/actions/dispatchActions';
import Aux from "../../../hoc/Aux";
import Modal from "../../../components/UI/Modal/Modal";
import Spinner from "../../../components/UI/Spinner/Spinner";
import PrescriptionSearchBar from '../../../components/UI/SearchBar/PrescriptionSearchBar';
import PrescriptionList from '../../../components/Lists/Prescription/PrescriptionListComp';
import CreatedPrescriptionFill from '../../../components/UI/Modal/CreatedPrescriptionFill/CreatedPrescriptionFill';


class PharmacistHome extends Component {
    state = {
        searchPid: '',
        pageSize: '10',
        error: null,
        prescriptionId: null,
        prescriptionFillDate: null,
        showPrescriptionFill: false
    };

    static defaultProps = {
        defaultUrl: 'http://localhost:8081/api/pharmacist/prescription/patient/'
    };

    componentDidMount() {
        const token = this.props.tokenRedux;
        const userPid = this.props.userPidRedux;
    }

    // changeHandler = (event) => {
    //     this.setState({ searchPid: event.target.value });
    //     console.log(this.state.searchPid);
    // }

    searchHandler = (value) => {
        const searchPid = value.pid;
        console.log(searchPid);
        const url = this.props.defaultUrl + searchPid;
        console.log("searchUrl:", url);
        const token = this.props.tokenRedux;
        const pageSize = this.state.pageSize;
        this.props.onFetchPrescriptions(url, token, pageSize);
    }

    prescriptionFillSubmit = (index) => {
        const date = moment().format("YYYY-MM-DD HH:mm:ss");
        this.setState({ showPrescriptionFill: !this.state.showPrescriptionFill, prescriptionId: index,prescriptionFillDate: date });

        // const
        const prescriptionFillId = {
            prescriptionId: this.props.prescriptionListRedux[index].prescriptionId
        };

        let postUrl = 'http://localhost:8081/api/pharmacist/new/prescription-fill/';
        const token = this.props.tokenRedux;
        if (token) {
            axios.post(postUrl, prescriptionFillId, { headers: { 'Authorization': 'Bearer ' + token } })
                .then(response => {
                    console.log("prescriptionFill sent");
                })
                .catch(error => {
                    console.log('prescriptionFill error ', error);
                })
        }
    };

    render() {
        const searchPid = this.state.searchPid;
        let prescriptionList = <Spinner />;

        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }

        if (this.props.loadingRedux) {
            prescriptionList = <Spinner />;
        }
        if (this.props.errorRedux) {
            prescriptionList = <h3>{this.props.errorRedux}</h3>
        }
        if (!this.props.tokenRedux) {
            prescriptionList = <Redirect to='/' />
        }
        else {
            prescriptionList = (
                <Aux>
                    <PrescriptionList
                        prescriptionList={this.props.prescriptionListRedux}
                        key={this.props.prescriptionListRedux.prescriptionId}
                        validUntil={this.props.prescriptionListRedux.validUntil}
                        prescribedOn={this.props.prescriptionListRedux.prescribedOn}
                        activeIngredient={this.props.prescriptionListRedux.activeIngredient}
                        prescriptionAction={this.prescriptionFillSubmit}
                        buttonTitle="Panaudoti" />
                </Aux>
            );
        }

        let prescriptionFillDetails = null;
        let validUntil = null;
        if (this.state.showPrescriptionFill) {
            const index = this.state.prescriptionId;
            const prescription = this.props.prescriptionListRedux[index];
            if (prescription.validUntil === null) {
                validUntil = "Neterminuotas";
                console.log("neterminuotas", prescription.validUntil);
            } else {
                validUntil = prescription.validUntil;
                console.log("paprastas", prescription.validUntil);
            }
            prescriptionFillDetails = <Modal show={this.state.showPrescriptionFill} hide={this.prescriptionFillSubmit}>
                <CreatedPrescriptionFill
                    prescribedOn={prescription.prescribedOn}
                    validUntil={validUntil}
                    activeIngredient={prescription.activeIngredient}
                    activeIngredientPerDose={prescription.activeIngredientPerDose}
                    activeIngredientUnits={prescription.activeIngredientUnits}
                    fillNum={prescription.prescriptionId}
                    lastFill={this.state.prescriptionFillDate}
                    hide={this.prescriptionFillSubmit}
                    back={this.prescriptionFill}>
                </CreatedPrescriptionFill>
            </Modal>
        }

        return (
            <Aux>
                <h3>Receptų paieška</h3>
                <div>
                    <PrescriptionSearchBar value={searchPid} onSubmit={this.searchHandler}> </PrescriptionSearchBar>
                </div>
                <br />
                <div>
                    {prescriptionList}
                    {prescriptionFillDetails}
                </div>
            </Aux>
        );
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        prescriptionListRedux: state.fetchPrescriptions.prescriptionList,
        loadingRedux: state.fetchPrescriptions.loading,
        errorRedux: state.fetchPrescriptions.error,
        // pasiimu tokena
        tokenRedux: state.auth.accessToken,
        userPidRedux: state.auth.userPid,
        totalPagesRedux: state.fetchPrescriptions.totalPages,
        totalElementsRedux: state.fetchPrescriptions.totalElements,
        pageSizeRedux: state.fetchPrescriptions.pageSize,
        pageNumberRedux: state.fetchPrescriptions.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchPrescriptions: (url, token, pageSize) => dispatch(actions.fetchPrescriptions(url, token, pageSize)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PharmacistHome);

