import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button } from 'react-bootstrap';
import {Redirect} from "react-router-dom";

import Aux from "../../../hoc/Aux";
import Spinner from "../../../components/UI/Spinner/Spinner";
import UserCard from '../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../assets/usericon.png';
import UserChangePassword from "../../Administration/UserAccount/UserChangePassword";

class PharmacistProfile extends Component {
    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: 0,
            organization: ''
        },
        showForm: false
    };

    componentDidMount() {
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            const authUser = this.props.userDetailsRedux;
            const updatedUser = { ...authUser, image: usericon, loading: false };
            this.setState({ user: updatedUser })
        }
    }

    changePasswordHandler = () => this.setState({ showForm: !this.state.showForm });

    render() {
        let changePasswordForm = null;

        if (this.state.showForm) {
            changePasswordForm = <UserChangePassword back={this.changePasswordHandler} />
        }
        let userCard = null;
        if (!this.props.userDetailsRedux) {
            userCard = <Redirect to='/pharmacist/home' />
        } else if (this.state.loading || this.props.loadingRedux) {
            userCard = <Spinner />
        } else if (this.state.error || this.props.errorRedux) {
            userCard = <h3>Tinklo klaida</h3>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                <p>{this.state.user.organization}</p>
                <p>
                    <Button id='pharmacistProfileChangePassword' bsStyle="info" onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(PharmacistProfile);

