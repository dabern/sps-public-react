import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import Aux from '../../../hoc/Aux';
import UserCard from '../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../assets/usericon.png';
import axios from 'axios'
import {connect} from "react-redux";
import UserChangePassword from "../../Administration/UserAccount/UserChangePassword";
import Spinner from "../../../components/UI/Spinner/Spinner";
import {Redirect} from "react-router-dom";

class DoctorProfile extends Component {
    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: 0,
            specializationId: '',
        },
        showForm: false,
        loading: true,
        error: false
    };

    componentDidMount() {
        let token = null;
        const specUrl = 'http://localhost:8081/api/specializations/';
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            token = this.props.tokenRedux;
            const authUser = this.props.userDetailsRedux;
            const spec = authUser.specializationId;
            axios.get(specUrl + spec, {headers: {'Authorization': 'Bearer ' + token}}).then(response => {
                const spec = response.data.title;
                console.log('Doctor specialization ', spec);
                const updatedUser = {...authUser, image: usericon, specializationId: spec};
                this.setState({user: updatedUser, loading: false})
            }).catch(() => {
                this.setState({error: true})
            })
        }
    }

    changePasswordHandler = () => this.setState({showForm: !this.state.showForm});

    render() {

        let changePasswordForm = null;
        if (this.state.showForm) {
            changePasswordForm = <UserChangePassword back={this.changePasswordHandler}/>
        }
        let userCard = null;
        if (!this.props.userDetailsRedux) {
            userCard = <Redirect to='/doctor/home'/>
        } else if (this.state.loading || this.props.loadingRedux) {
            userCard = <Spinner/>
        } else if (this.state.error || this.props.errorRedux) {
            userCard = <h3>Tinklo klaida</h3>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                <span id='doctorProfileSpecialization'><strong>{this.state.user.specializationId}</strong></span>
                <p>
                    <Button id='doctorProfileChangePassword' bsStyle="info" onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
                {/* <div>DoctorStatistics</div> */}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(DoctorProfile);
