import React, {Component} from 'react';
import {Link, Redirect} from "react-router-dom";
import Aux from "../../../../hoc/Aux";
import UserCard from '../../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../../assets/usericon-guy.png';
import {Button} from 'react-bootstrap';
import MedicalHistoryList from "../../../Lists/MedicalHistoryList/MedicalHistoryList";
import PrescriptionList from "../../../Lists/PrescriptionList/PrescriptionList";
import {connect} from "react-redux";

class DoctorPatientDetails extends Component {
    // Container that renders patient's info once the doctor clicks patient details in the PatientList

    state = {
        image: usericon,
    };

    render() {

        let userCard = <Redirect to='/doctor/home'/>;
        if (this.props.userRedux.length > 0) {
            console.log(this.props.userRedux);
            const patient = this.props.match.params.pid;
            const userIndex = this.props.userRedux.findIndex(x => x.pid.toString() === patient);
            const myPatient = this.props.userRedux[userIndex];
            const buttonStyle = {width: '150px'};
            userCard = (<div>
                <UserCard
                    image={this.state.image}
                    firstName={myPatient.firstName}
                    lastName={myPatient.lastName}
                    pid={myPatient.pid}>
                    <p>{myPatient.dob}</p>
                    <p><Link to={"/doctor/newrecord/" + userIndex}>
                        <Button bsStyle="info" style={buttonStyle}>
                            Naujas ligos įrašas
                        </Button>
                    </Link></p>
                    <p><Link to={"/doctor/newprescription/" + userIndex}>
                        <Button bsStyle="info" style={buttonStyle}>
                            Naujas receptas
                        </Button>
                    </Link></p>
                </UserCard>
                <h3>Ligos istorija</h3>
                <MedicalHistoryList/>
                <h3>Išrašyti receptai</h3>
                <PrescriptionList/>
            </div>)

        }

        return (
            <Aux>
                {userCard}
            </Aux>

        );
    }
}

const mapStateToProps = state => {
    return {
        userRedux: state.fetch.userList,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(DoctorPatientDetails);
