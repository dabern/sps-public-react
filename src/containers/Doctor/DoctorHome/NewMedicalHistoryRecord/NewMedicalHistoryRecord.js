import React, { Component } from 'react';
import axios from 'axios/index';
import NewMedicalHistoryRecordForm from "../../../../components/Form/Forms/MedicalHistoryRecordForm";

class NewMedicalHistoryRecord extends Component {
    // Renders the MedicalHistoryRecordForm.js once the doctor clicks "Naujas ligos įrašas" on his patient's UserCard (at the moment see DoctorPatientDetails.js)
    // Should probably use componentDidMount here to load the values into the form

    newMedicalHistoryRecordCreationHandler = (values) => {
        //shouldn't this have an ID?
        axios.post("http://localhost:8081/api/medicalRecords/new", values, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('access_token') } })
            .then(response => {
                console.log('medHistRecord creationHandler response ',response.data)
            }).catch(error => {
                console.log('medHistRecord creationHandler response ',error)
            });
        window.alert(`Created New Medical History Record:\n\n${JSON.stringify(values, null, 2)}`)
    };

    render() {
        return (
            <NewMedicalHistoryRecordForm
                onSubmit={this.newMedicalHistoryRecordCreationHandler}
                back={() => this.props.history.push('/doctorpatientdetails')} />
        );
    }
}

export default NewMedicalHistoryRecord;
