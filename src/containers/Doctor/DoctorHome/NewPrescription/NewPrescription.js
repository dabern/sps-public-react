import React, { Component } from 'react';
import axios from 'axios/index';
import PrescriptionForm from '../../../../components/Form/Forms/PrescriptionForm';

class Prescription extends Component {
    state = {
        state = {
            isCreated: false,
            pid: 0
        }
    }
    // Container that renders the PrescriptionForm.js once the doctor clicks the "Naujas receptas" button (see DoctorPatientDetails.js)
    prescriptionCreationHandler = (values) => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/doctor/new/prescription';
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined') {
            url = 'http://localhost:8081/api/admin/pid/' + number + '/update';
            this.props.onUpdateUser(url, values, token);
        } else {
            this.props.onCreateUser(url, values, token);
            this.props.afterSubmit();
        }

        window.alert(`Created New Prescription:\n\n${JSON.stringify(values, null, 2)}`)
    };

    componentDidMount() {
        const number = this.props.match.params;
        console.log('NewPrescription didMount number', number)
    }

    render() {
        return (
            <NewPrescriptionForm

                onSubmit={this.prescriptionCreationHandler}
                back={() => this.props.history.push('/doctorpatientdetails')}>
                firstName={this.state.firstName}
            </NewPrescriptionForm>

        );
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token) => dispatch(actions.userCreation(url, userData, token)),
        onUpdateUser: (url, userData, token) => dispatch(actions.userUpdate(url, userData, token)),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        afterSubmit: () => dispatch(reset('AdminForm'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Prescription);
