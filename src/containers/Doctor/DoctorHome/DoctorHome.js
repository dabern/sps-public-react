import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
// import { Button } from 'react-bootstrap';
import Aux from '../../../hoc/Aux';
import PatientList from "../../../containers/Lists/PatientList/PatientList";

class DoctorHome extends Component {

    render() {
        return (
            <Aux>
                {/* http://localhost:8081/patient/2/all  */}
                <PatientList />
                {/* <Link to="/doctor/patientdetails:pid">
                <Button>
                    Paciento informacija
                </Button>
                </Link> */}
            </Aux>
        );
    }
}

export default DoctorHome;
