import React, {Component} from 'react';
import PrescriptionForm from '../../../../components/Form/Forms/PrescriptionForm';
import Aux from "../../../../hoc/Aux";
import {initialize} from "redux-form";
import * as actions from "../../../../store/actions/dispatchActions";
import {connect} from "react-redux";
import CreatedPrescription from "../../../../components/UI/Modal/CreatedPrescription/CreatedPrescription";
import Modal from "../../../../components/UI/Modal/Modal";
import icon from '../../../../assets/usericon-guy.png';
import moment from "moment/moment";

class Prescription extends Component {
    state = {
        isCreated: false,
        image: icon,
    };

    componentWillMount() {
        const url = 'http://localhost:8081/api/ingredients/search/by-title';
        const token = this.props.tokenRedux;
        this.props.onFetchIngredients(url, token, 3, 'A');
        this.props.onInitCreation();
    }

    componentDidMount() {
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined' && this.props.userRedux && this.props.authDoctor) {
            const values = this.props.userRedux[number];
            const doctorId = this.props.authDoctor.id;
            const data = {...values, patientPid: values.pid, doctorId: doctorId};
            this.props.onLoad(data);
        } else {
            this.props.history.replace('/doctor/newprescription')
        }
    }

    prescriptionCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/doctor/new/prescription';
        const token = this.props.tokenRedux;
        const formName = 'PrescriptionForm';
        const type = 'kazkokia klaida';
        if (!values.dosageNotes) {
            values.dosageNotes = 'Nėra';
        }
        if (values.validUntil && values.unlimitedValidity) {
            values.validUntil = null;
        } else if (values.validUntil) {
            values.validUntil = moment(values.validUntil).format("YYYY-MM-DD HH:mm");
        }
        this.props.onCreateUser(url, values, token, formName, type);
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    filterHandler = (event, values) => {
        const token = this.props.tokenRedux;
        const url = 'http://localhost:8081/api/ingredients/search/by-title';
        const pageSize = '3';
        if (values.length > 2) {
            this.props.onFetchIngredients(url, token, pageSize, values);
        } else if (values.length === 0) {
            this.props.onFetchIngredients(url, token, pageSize, values);
        }
    };

    render() {

        const myPatient = typeof this.props.match.params.id !== 'undefined';
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            let validUntil = this.props.createdUserDataRedux.validUntil;
            if (!validUntil) {
                validUntil = 'Neterminuotas'
            }
            createdUser = <Modal
                show={this.state.isCreated}
                hide={() => this.props.history.replace('/doctor/newprescription')}>
                <CreatedPrescription
                    image={this.state.image}
                    firstName={this.props.createdUserDataRedux.firstName}
                    lastName={this.props.createdUserDataRedux.lastName}
                    pid={this.props.createdUserDataRedux.pid}
                    activeIngredient={this.props.createdUserDataRedux.activeIngredient}
                    activeIngredientPerDose={this.props.createdUserDataRedux.activeIngredientPerDose}
                    activeIngredientUnits={this.props.createdUserDataRedux.activeIngredientUnits}
                    validUntil={validUntil}
                    prescribedOn={new Date().toLocaleDateString("lt-LT")}
                    dosageNotes={this.props.createdUserDataRedux.dosageNotes}
                    hide={() => this.props.history.replace('/doctor/newprescription')}
                    back={this.props.history.goBack}/>
            </Modal>
        }

        let ingredients = null;
        let ingredientTitles = null;
        if (this.props.ingredientsListRedux) {
            ingredients = this.props.ingredientsListRedux;
            ingredientTitles = ingredients.map(ingredient => {return {id: ingredient.id, title: ingredient.title}})
        }
        // console.log('ingredient prescription render', onlyTitles);

        return (
            <Aux>
                {createdUser}
                <PrescriptionForm
                    ingredientChange={this.filterHandler}
                    onSubmit={this.prescriptionCreationHandler}
                    back={this.props.history.goBack}
                    myPatient={myPatient}
                    ingredient={ingredientTitles}>
                    {errorMessage}
                </PrescriptionForm>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        // createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        userRedux: state.fetch.userList,
        authDoctor: state.auth.authUser,
        ingredientsListRedux: state.fetchOther.otherList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onLoad: values => dispatch(initialize('PrescriptionForm', values)),
        onFetchIngredients: (url, token, pageSize, searchParams) => dispatch(actions.fetchOthers(url, token, pageSize, searchParams)),

}
};

export default connect(mapStateToProps, mapDispatchToProps)(Prescription);
