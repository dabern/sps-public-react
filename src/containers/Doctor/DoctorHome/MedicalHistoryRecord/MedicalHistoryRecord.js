import React, {Component} from 'react';
import MedicalHistoryRecordForm from "../../../../components/Form/Forms/MedicalHistoryRecordForm";
import Aux from "../../../../hoc/Aux";
import {connect} from "react-redux";
import {initialize} from 'redux-form';
import * as actions from "../../../../store/actions/dispatchActions";
import Modal from "../../../../components/UI/Modal/Modal";
import CreatedVisit from "../../../../components/UI/Modal/CreatedVisit/CreatedVisit";
import icon from '../../../../assets/usericon-guy.png';
import moment from 'moment';

class MedicalHistoryRecord extends Component {
    // Renders the MedicalHistoryRecordForm.js once the doctor clicks "Naujas ligos įrašas" on his patient's UserCard (at the moment see DoctorPatientDetails.js)
    // Should probably use componentDidMount here to load the values into the form

    state = {
        isCreated: false,
        image: icon
    };

    componentWillMount() {
        const url = 'http://localhost:8081/api/diagnoses/search/by-title';
        const token = this.props.tokenRedux;
        this.props.onFetchDiagnoses(url, token, 3, 'A');
        this.props.onInitCreation();
    }

    componentDidMount() {
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined' && this.props.userRedux && this.props.authDoctor) {
            const values = this.props.userRedux[number];
            const doctorId = this.props.authDoctor.id;
            const data = {...values, patientPid: values.pid, doctorId: doctorId};
            this.props.onLoad(data);
            console.log('medHistoryDid', data);
        } else {
            this.props.history.replace('/doctor/newrecord')
        }
    }

    medicalHistoryRecordCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'http://localhost:8081/api/doctor/new/medical-record';
        const token = this.props.tokenRedux;
        const formName = 'MedicalHistoryRecordForm';
        const type = 'Klaida ';
        if(!values.repeatVisitation){
            values.repeatVisitation = false;
        }
        if(!values.compensated) {
            values.compensated = false;
        }
        if(!values.notes) {
            values.notes = 'Nėra pastabų';
        }
        console.log('medicalRecordCreation', values);
        this.props.onCreateUser(url, values, token, formName, type);
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    filterHandler = (event, values) => {
        const token = this.props.tokenRedux;
        const url = 'http://localhost:8081/api/diagnoses/search/by-title';
        const pageSize = '3';
        if (values.length > 1) {
            this.props.onFetchDiagnoses(url, token, pageSize, values);
        } else if (values.length === 0) {
            this.props.onFetchDiagnoses(url, token, pageSize, values);
        }
    };

    render() {

        const myPatient = typeof this.props.match.params.id !== 'undefined';
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        const date = moment(new Date()).format("YYYY-MM-DD HH:mm");
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            createdUser = <Modal
                show={this.state.isCreated}
                hide={() => this.props.history.replace('/doctor/newrecord')}>
                <CreatedVisit
                    image={this.state.image}
                    firstName={this.props.createdUserDataRedux.firstName}
                    lastName={this.props.createdUserDataRedux.lastName}
                    pid={this.props.createdUserDataRedux.pid}
                    visitDate={date}
                    diagnosis={this.props.createdUserDataRedux.diagnosisId}
                    visitLength={this.props.createdUserDataRedux.appointmentLength}
                    notes={this.props.createdUserDataRedux.notes}
                    hide={() => this.props.history.replace('/doctor/newrecord')}
                    back={this.props.history.goBack}/>
            </Modal>
        }

        let diagnoses = null;
        let diagnosisTitles = null;
        if (this.props.diagnosesListRedux) {
            diagnoses = this.props.diagnosesListRedux;
            diagnosisTitles = diagnoses.map(diagnosis => {return {id: diagnosis.id, title: diagnosis.title}})
        }

        return (
            <Aux>
                {createdUser}
                <MedicalHistoryRecordForm
                    diagnosisChange={this.filterHandler}
                    diagnosis={diagnosisTitles}
                    onSubmit={this.medicalHistoryRecordCreationHandler}
                    back={this.props.history.goBack}
                    myPatient={myPatient}>
                    {errorMessage}
                </MedicalHistoryRecordForm>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        loadingRedux: state.create.loading,
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        userRedux: state.fetch.userList,
        authDoctor: state.auth.authUser,
        diagnosesListRedux: state.fetchOther.otherList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onLoad: values => dispatch(initialize('MedicalHistoryRecordForm', values)),
        onFetchDiagnoses: (url, token, pageSize, searchParams) => dispatch(actions.fetchOthers(url, token, pageSize, searchParams)),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MedicalHistoryRecord);
