import React, {Component} from "react";
import "./Home.css";
import LoginButton from "../../components/UI/Buttons/LoginButton/LoginButton";
import {connect} from "react-redux";

class Home extends Component {

    render() {

        const homePage = (
            <div>
                <h3 id='HomePageHeader' className="LoginHeader">Prisijunkite prie savo paskyros:</h3>
                <LoginButton/>
            </div>
        );
        return (
            <div>
                {homePage}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticatedRedux: state.auth.accessToken
    }
};

export default connect(mapStateToProps)(Home)
