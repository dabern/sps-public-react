import React, {Component} from "react";
import {Route, Switch, withRouter} from "react-router-dom";
import Home from "./containers/Homepage/Home";
import Login from "./containers/LoginPage/Login";
import NotFound from "./containers/NotFound";
import AccountCreation from "./containers/Administration/UserAccount/AccountCreation";
import PatientCreation from "./containers/Administration/UserCreation/PatientCreation";
import DoctorCreation from "./containers/Administration/UserCreation/DoctorCreation";
import PharmacistCreation from "./containers/Administration/UserCreation/PharmacistCreation";
import AdminCreation from "./containers/Administration/UserCreation/AdminCreation";
import AdminProfile from "./containers/Administration/AdminProfile";
import DoctorHome from "./containers/Doctor/DoctorHome/DoctorHome";
import DoctorProfile from "./containers/Doctor/DoctorProfile/DoctorProfile";
import DoctorPatientDetails from "./containers/Doctor/DoctorHome/DoctorPatientDetails/DoctorPatientDetails";
import AdminUserList from "./containers/Administration/AdminUserList";
import PatientHome from "./containers/Patient/PatientHome/PatientHome";
import PatientProfile from "./containers/Patient/PatientProfile/PatientProfile";
import PharmacistProfile from "./containers/Pharmacist/PharmacistProfile/PharmacistProfile";
import PharmacistHome from "./containers/Pharmacist/PharmacistHome/PharmacistHome";
import UserChangePassword from "./containers/Administration/UserAccount/UserChangePassword";
import MedicalHistoryRecord from "./containers/Doctor/DoctorHome/MedicalHistoryRecord/MedicalHistoryRecord";
import Prescription from "./containers/Doctor/DoctorHome/Prescription/Prescription";
import Logout from "./containers/LoginPage/Logout";
import UserLayout from "./components/Navigation/Sidebar/UserLayout";
import {connect} from "react-redux";

class Routes extends Component {

    state = {
        isShow: false
    };

    isShowHandler = () => this.setState({isShow: !this.state.isShow});

    render(){

        let routes = <Switch>
            <Route path="/login" exact component={Login}/>
            <Route path="/" exact component={Home}/>
            <Route component={NotFound}/>
        </Switch>;

        if(this.props.isAuthenticatedRedux){
            routes = <Switch>
                <UserLayout show={this.state.isShow} clicked={this.isShowHandler}>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/logout" exact component={Logout}/>
                    <Route path="/admin/profile" exact component={AdminProfile}/>
                    <Route path="/admin/register/patient" exact component={PatientCreation}/>
                    <Route path="/admin/register/patient/:id" exact component={PatientCreation}/>
                    <Route path="/admin/register/patient/account/:id" exact component={AccountCreation}/>
                    <Route path="/admin/register/doctor" exact component={DoctorCreation}/>
                    <Route path="/admin/register/doctor/:id" exact component={DoctorCreation}/>
                    <Route path="/admin/register/pharmacist" exact component={PharmacistCreation}/>
                    <Route path="/admin/register/pharmacist/:id" exact component={PharmacistCreation}/>
                    <Route path="/admin/register/admin" exact component={AdminCreation}/>
                    <Route path="/admin/register/admin/:id" exact component={AdminCreation}/>
                    <Route path="/admin/userList" exact component={AdminUserList}/>
                    <Route path="/doctor/home" exact component={DoctorHome}/>
                    <Route path="/doctor/profile" exact component={DoctorProfile}/>
                    <Route path="/doctor/patientdetails/:pid" exact component={DoctorPatientDetails}/>
                    <Route path="/patient/profile" exact component={PatientProfile}/>
                    <Route path="/patient/home" exact component={PatientHome}/>
                    <Route path="/pharmacist/profile" exact component={PharmacistProfile}/>
                    <Route path="/pharmacist/home" exact component={PharmacistHome}/>
                    <Route path="/pharmacist/home/:pid" exact component={PharmacistHome}/>
                    <Route path="/changepassword" exact component={UserChangePassword}/>
                    <Route path="/doctor/newrecord" exact component={MedicalHistoryRecord}/>
                    <Route path="/doctor/newrecord/:id" exact component={MedicalHistoryRecord}/>
                    <Route path="/doctor/newprescription" exact component={Prescription}/>
                    <Route path="/doctor/newprescription/:id" exact component={Prescription}/>
                    {/*<Route path="/" exact component={Home}/>*/}
                    {/*<Redirect to="/"/>*/}
                </UserLayout>
            </Switch>
        }
        return <div>{routes}</div>
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticatedRedux: state.auth.accessToken
    }
};

export default withRouter(connect(mapStateToProps)(Routes));
