import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { renderTextField } from './renderFields'
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css';
import {Paper} from "material-ui";

const loginForm = props => {
    const { handleSubmit, pristine, submitting, reset } = props;
    return (
        <form id='loginForm' onSubmit={handleSubmit}>
            <h3 id='loginFormHeader'>Prisijungimas prie sistemos</h3>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <Field autoFocus={true} name="username" component={renderTextField} label="Prisijungimo vardas" />
                <Field name="password" type="password" component={renderTextField} label="Slaptažodis" />
                <div id='loginFormError' className='error'>{props.children}</div>
            </Paper>
            <div>
                <Button id='loginFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Prisijungti</Button>
                {' '}
                <Button id='loginFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={pristine || submitting} onClick={reset}>Atšaukti</Button>
            </div>
        </form>
    )
};

export default reduxForm({
    form: 'LoginForm', // a unique identifier for this form
    validate
})(loginForm)
