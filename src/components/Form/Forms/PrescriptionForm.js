import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import {MenuItem, Paper} from 'material-ui';
import { renderTextField, renderMultiLineTextField, renderSelectField, renderDatePickerField } from './../../Form/Forms/renderFields';
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import Aux from "../../../hoc/Aux";
import {onlyNumbers, toFirstUpperCaseToLowerCase} from '../../../shared/utility';
import {renderAutoComplete, renderCheckbox} from "./renderFields";

let prescriptionForm = (props) => {


    // Both dates and activeIngredientPerDose & activeIngredientUnits should be on the same line, respectively; to be fixed later
    const { handleSubmit, pristine, submitting } = props;
    return (
        <Aux>
            <form id='prescriptionForm' onSubmit={handleSubmit}>
                <h3 id='prescriptionFormHeader'>Naujas receptas</h3>
                <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                    <Field name="firstName" autoFocus={!props.myPatient} normalize={toFirstUpperCaseToLowerCase} disabled={props.myPatient} component={renderTextField} label="Paciento vardas"/>
                    <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} disabled={props.myPatient} component={renderTextField} label="Paciento pavardė" />
                    <Field name="patientPid" normalize={onlyNumbers} disabled={props.myPatient} maxLength='11' component={renderTextField} label="Paciento asmens kodas" />
                    <Field autoFocus={props.myPatient} name="activeIngredient" onChange={(event, value) => props.ingredientChange(event, value)}
                           component={renderAutoComplete} label="Vaisto veiklioji medžiaga" dataSourceConfig={{text: 'title', value: 'id'}} dataSource={[].concat(props.ingredient)} />
                    <Field name="activeIngredientPerDose" normalize={onlyNumbers} component={renderTextField} label="Medžiagos kiekis dozėje" />
                    <Field style={{ textAlign: 'left' }} name="activeIngredientUnits" component={renderSelectField} label="Matavimo vienetas">
                        <MenuItem id='prescriptionFormActiveIngredientPerDoseMg' value="mg" primaryText="mg" />
                        <MenuItem id='prescriptionFormActiveIngredientPerDoseµg' value="µg" primaryText="µg" />
                        <MenuItem id='prescriptionFormActiveIngredientPerDoseUi' value="TV/IU" primaryText="TV/IU" />
                    </Field>
                    <Field name="validUntil" minDate={new Date()} component={renderDatePickerField} label="Galioja iki:" />
                    <Field name="unlimitedValidity" component={renderCheckbox} label="Neterminuotas?"/>
                    <Field style={{ textAlign: 'left' }} name="dosageNotes" maxLength='8000' component={renderMultiLineTextField} label="Vartojimo aprašymas" />
                    <div id='prescriptionFormError' className='error'>{props.children}</div>
                </Paper>
                <div>
                    <Button id='prescriptionFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Registruoti</Button>
                    {' '}
                    <Button id='prescriptionFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.back}>Atšaukti</Button>
                </div>
            </form>
        </Aux>
    )
};


const mapStateToProps = (state, index) => {
    if (state.fetch.userList){
        return {
            initialValues: state.fetch.userList[index] // pull initial values from account reducer
        }
    }
    return {};
};

prescriptionForm = reduxForm({ form: 'PrescriptionForm', validate })(prescriptionForm);
export default connect(mapStateToProps)(prescriptionForm)

