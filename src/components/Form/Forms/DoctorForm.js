import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { renderTextField, renderAutoComplete } from './renderFields';
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css';
import { connect } from "react-redux";
import {Paper} from 'material-ui';
import Aux from "../../../hoc/Aux";
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../../shared/utility'

let doctorForm = props => {

    const {handleSubmit, pristine, submitting, reset } = props;
    return (
        <form id='doctorForm' onSubmit={handleSubmit}>
            <h3 id='doctorFormHeader'>{props.created ? 'Gydytojo registracija' : 'Atnaujinti gydytojo duomenis'}</h3>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <Field autoFocus={true} name="firstName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Vardas" />
                <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Pavardė" />
                <Field name="pid" maxLength='11' normalize={onlyNumbers} component={renderTextField} label="Asmens kodas" />
                <Field name="specializationId" component={renderAutoComplete} label="Specializacija" hintText='Jei nėra, įrašykite "Kita"' dataSourceConfig={{text: 'title', value: 'id'}} dataSource={[].concat(props.spec).concat([{id: 0, title: 'Kita'}])} />
                {props.created ? <Aux><Field name="username" component={renderTextField} label="Prisijungimo vardas" />
                <Field name="password" type='password' component={renderTextField} label="Slaptažodis"/></Aux> : null}
                <div id='doctorFormError' className='error'>{props.children}</div>
            </Paper>
            {props.showButtons ?
            <Aux>
                <Button id='doctorFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>{props.created ? 'Registruoti':'Atnaujinti'}</Button>
                {' '}
                <Button id='doctorFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={props.created ? pristine || submitting : submitting} onClick={props.created ? reset : props.back}>Atšaukti</Button></Aux> : null}
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.getUser.userById // pull initial values from account reducer
    }
};

doctorForm = reduxForm({ form: 'DoctorForm', validate})(doctorForm);
export default connect(mapStateToProps)(doctorForm)
