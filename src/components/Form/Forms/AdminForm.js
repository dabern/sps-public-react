import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderTextField} from './renderFields'
import {Button} from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css'
import {connect} from "react-redux";
import {Paper} from 'material-ui';
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../../shared/utility'
import Aux from "../../../hoc/Aux";

let adminForm = props => {
    const {handleSubmit, pristine, submitting, reset} = props;
    return (
        <form id='adminForm' onSubmit={handleSubmit}>
            <h3 id='adminFormHeader'>{props.created ? 'Administratoriaus registracija' : 'Atnaujinti administratoriaus duomenis'}</h3>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <Field autoFocus={true} name="firstName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Vardas"/>
                <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Pavardė"/>
                <Field name="pid" normalize={onlyNumbers} maxLength='11' component={renderTextField} label="Asmens kodas"/>
                {props.created ? <Aux><Field name="username" component={renderTextField} label="Prisijungimo vardas" />
                <Field name="password" type='password' component={renderTextField} label="Slaptažodis"/></Aux> : null}
                <div id='adminFormError' className='error'>{props.children}</div>
            </Paper>
            <div>
                <Button id='adminFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>{props.created ? 'Registruoti' : 'Atnaujinti'}</Button>
                {' '}
                <Button id='adminFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={props.created ? pristine || submitting : submitting} onClick={props.created ? reset : props.back}>Atšaukti</Button>
            </div>
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.getUser.userById // pull initial values from account reducer
    }
};

adminForm = reduxForm({form: 'AdminForm', validate})(adminForm);
export default connect(mapStateToProps)(adminForm)
