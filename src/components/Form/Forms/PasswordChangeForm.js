import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderTextField} from './renderFields'
import {Button} from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css'
import {Paper} from "material-ui";
import {connect} from "react-redux";

let passwordChangeForm = props => {
    const {handleSubmit, pristine, submitting} = props;
    return (
        <form id='passwordChangeForm' onSubmit={handleSubmit}>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <h3 id='passwordChangeFormHeader'>Slaptažodžio keitimo forma</h3>
                {/*<Field autoFocus={true} name="password" type="password" component={renderTextField} label="Dabartinis slaptažodis"/>*/}
                <Field name="username" disabled={true} component={renderTextField} label="Vartotojo vardas"/>
                <Field autoFocus={true} name="password" type="password" hintText="Įveskite dabartinį slaptažodį" component={renderTextField} label="Slaptažodis"/>
                <Field name="newPassword" type="password" hintText="Įveskite naują slaptažodį" component={renderTextField} label="Naujas slaptažodis"/>
                <Field name="newPasswordConfirmation" type="password" hintText="Įveskite naują slaptažodį" component={renderTextField} label="Pakartokite naują slaptažodį"/>
                <div id='passwordChangeFormError' className='error'>{props.children}</div>
                <div id='passwordChangeFormButtons'>
                    <Button id='passwordChangeFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Keisti slaptažodį</Button>
                    {' '}
                    <Button id='passwordChangeFormDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.back}>Atšaukti</Button>
                </div>
            </Paper>
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.auth.authUser // pull initial values from account reducer
    }
};

passwordChangeForm = reduxForm({form: 'PasswordChangeForm', validate})(passwordChangeForm);
export default connect(mapStateToProps)(passwordChangeForm)
