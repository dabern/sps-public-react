import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderTextField} from './renderFields'
import {Button} from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css'
import {Paper} from 'material-ui';

const specializationForm = props => {
    const {handleSubmit, pristine, submitting} = props;
    return (
        <form id='specializationForm' onSubmit={handleSubmit}>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <h3 id='specializationFormHeader'>Įrašyti naują specializaciją</h3>
                <Field autoFocus={true} name="title" component={renderTextField} label="Specializacijos pavadinimas"/>
                <Field name="description" maxLength='8000' component={renderTextField} label="Specializacijos aprašymas"/>
                <div style={{margin: '30px'}}>
                    <Button id='specializationFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Įrašyti</Button>&emsp;
                    <Button id='specializationFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.back}>Atšaukti</Button>
                </div>
                <div id='specializationFormError' className='error'>{props.children}</div>
            </Paper>
        </form>
    )
};

export default reduxForm({form: 'SpecializationForm', validate})(specializationForm);
