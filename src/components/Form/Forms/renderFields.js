import React from 'react'
import { TextField, SelectField, DatePicker, Checkbox, AutoComplete } from 'material-ui'

export const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
    <div>
        <TextField
            hintText={label}
            floatingLabelText={label}
            errorText={touched && error}
            id={'form'+label}
            {...input}
            {...custom}
        />
    </div>
);

export const renderSelectField = ({ input, label, meta: { touched, error }, children, ...custom }) => (
    <div>
        <SelectField
            id={'form'+label}
            floatingLabelText={label}
            errorText={touched && error}
            {...input}
            onChange={(event, index, value) => input.onChange(value)}
            children={children}
            {...custom}
        />
    </div>
);
let DateTimeFormat = global.Intl.DateTimeFormat;

export const renderDatePickerField = ({ input, label, meta: { touched, error }, ...custom }) => (
    <div>
        <DatePicker
            // style={css}
            id={'form'+label}
            autoOk={true}
            hintText={label}
            floatingLabelText={label}
            DateTimeFormat={DateTimeFormat}
            errorText={touched && error}
            okLabel="Gerai"
            cancelLabel="Atšaukti"
            locale="lt"
            // container="inline"
            {...input}
            {...custom}
            // mode="landscape"
            // openToYearSelection={true}
            // value={{}} // if added removes error but doesn't display chosen value
            onChange={(event, value) => input.onChange(value)}
        />
    </div>
);

export const renderMultiLineTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
    <div>
        <TextField
            id={'form'+label}
            hintText={label}
            floatingLabelText={label}
            errorText={touched && error}
            multiLine={true}
            rows={4}
            rowsMax={8}
            {...input}
            {...custom}
        />
    </div>
);

export const renderCheckbox = ({ input, label }) => (
    <div style={{width: '256px', margin: '20px auto 0 auto', textAlign: 'left', fontSize: '16px'}}>
        <Checkbox
            labelStyle={{color: '#777'}}
            id={'form'+label}
            label={label}
            checked={!!input.value}
            onCheck={input.onChange}
            labelPosition="left"
        />
    </div>
);

export const renderAutoComplete = ({ input, label, meta: { touched, error }, ...custom }) => (
    <div>
        <AutoComplete
            id={'form'+label}
            floatingLabelText={label}
            errorText={touched && error}
            {...input}
            {...custom}
            // searchText={input.value}
            onUpdateInput={(e) => input.onChange(e)}
            onNewRequest={(selectedItem) => {input.onChange(selectedItem.id)}}
            filter={AutoComplete.caseInsensitiveFilter}
            maxSearchResults={3}
        />
    </div>
);
