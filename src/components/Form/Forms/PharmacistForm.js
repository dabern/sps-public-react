import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { renderTextField, renderSelectField } from './renderFields'
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import { MenuItem, Paper } from 'material-ui';
import './UserForms.css';
import { connect } from "react-redux";
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../../shared/utility'
import Aux from "../../../hoc/Aux";

let pharmacistForm = props => {
    const { handleSubmit, pristine, submitting, reset } = props;
    return (
        <form id='PharmacistForm' onSubmit={handleSubmit}>
            <h3 id='PharmacistFormHeader'>{props.created ? 'Vaistininko registracija' : 'Atnaujinti vaistininko duomenis'}</h3>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <Field autoFocus={true} name="firstName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Vardas" />
                <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Pavardė" />
                <Field name="pid" normalize={onlyNumbers} maxLength='11' component={renderTextField} label="Asmens kodas" />
                    <Field style={{ textAlign: 'left' }} name="type" component={renderSelectField} label="Įmonės teisinė forma">
                        <MenuItem id='PharmacistFormTypeAB' value="AB" primaryText="Akcinė bendrovė" />
                        <MenuItem id='PharmacistFormTypeMB' value="MB" primaryText="Mažoji bendrija" />
                        <MenuItem id='PharmacistFormTypeUAB' value="UAB" primaryText="Uždaroji akcinė bendrovė" />
                        <MenuItem id='PharmacistFormTypeVSI' value="VšĮ" primaryText="Viešoji Įstaiga" />
                    </Field>
                <Field name="organization" component={renderTextField} label="Įmonės pavadinimas" />
                {props.created ? <Aux><Field name="username" component={renderTextField} label="Prisijungimo vardas" />
                <Field name="password" type='password' component={renderTextField} label="Slaptažodis"/></Aux> : null}
                <div id='PharmacistFormError' className='error'>{props.children}</div>
            </Paper>
            <div>
                <Button id='pharmacistFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>{props.created ? 'Registruoti':'Atnaujinti'}</Button>
                {' '}
                <Button id='pharmacistFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={props.created ? pristine || submitting : submitting} onClick={props.created ? reset : props.back}>Atšaukti</Button>
            </div>
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.getUser.userById // pull initial values from account reducer
    }
};

pharmacistForm = reduxForm({ form: 'PharmacistForm', validate })(pharmacistForm);
export default connect(mapStateToProps)(pharmacistForm)
