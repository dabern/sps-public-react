import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { renderTextField } from './renderFields'
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css'
import { connect } from "react-redux";
import {Paper} from 'material-ui';
import {toFirstUpperCaseToLowerCase, onlyNumbers, stringToDate} from '../../../shared/utility'

let patientForm = props => {
    const { handleSubmit, pristine, submitting, reset } = props;
    return (
        <form id='patientForm' onSubmit={handleSubmit}>
            <h3 id='patientFormHeader'>{props.created ? 'Paciento registracija' : 'Atnaujinti paciento duomenis'}</h3>
            <Paper zDepth={2} style={{width: '400px', margin: '30px auto', padding: '10px 15px 40px 15px'}}>
                <Field autoFocus={props.created} name="firstName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Vardas" />
                <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Pavardė" />
                <Field autoFocus={!props.created} name="pid" normalize={onlyNumbers} maxLength='11' component={renderTextField} label="Asmens kodas" />
                <Field name="dob" normalize={stringToDate} hintText='YYYY-MM-DD' component={renderTextField} label="Gimimo data" />
                <Field name="doctorPidNullable" normalize={onlyNumbers} maxLength='11' component={renderTextField} label="Gydytojo asmens kodas" />
                <div id='patientFormError' className='error'>{props.children}</div>
            </Paper>
            <div>
                <Button id='patientFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>{props.created ? 'Registruoti':'Atnaujinti'}</Button>
                {' '}
                <Button id='patientFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={props.created ? pristine || submitting : submitting} onClick={props.created ? reset : props.back}>Atšaukti</Button>
            </div>
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.getUser.userById // pull initial values from account reducer
    }
};

patientForm = reduxForm({ form: 'PatientForm', validate })(patientForm);
export default connect(mapStateToProps)(patientForm)
