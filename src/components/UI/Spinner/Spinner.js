import React from 'react';
import './Spinner.css'

const spinner = () => (
<div className="loader">Kraunasi...</div>
);

export default spinner;
