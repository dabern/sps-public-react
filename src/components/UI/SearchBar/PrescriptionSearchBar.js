import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { renderTextField } from '../../../components/Form/Forms/renderFields'
import { Button } from 'react-bootstrap';
import validate from "../../../shared/validate";
import { onlyNumbers } from '../../../shared/utility'

const PrescriptionSearchBar = props => {
    const { handleSubmit, pristine, submitting, value } = props;
    return (
        <form id="prescriptionSearchForm" onSubmit={props.handleSubmit}>
            <Field
                name="pid"
                id="pharmacistPatientPidSearch"
                component={renderTextField}
                value={value}
                label="Įveskite asmens kodą"
                style={{ width: '350px' }}
                autoFocus={true}
                // normalize={onlyNumbers}
                maxLength='11'
                // onChange={changeHandler}
            />
            {' '}
            <Button bsStyle="info" type="submit" id="pharmacistPatientPidSearchButton" disabled={pristine || submitting}>
                Ieškoti
            </Button>
        </form>
    );
};
export default PrescriptionSearchBar = reduxForm({ form: 'PrescriptionSearchBar', validate })(PrescriptionSearchBar)