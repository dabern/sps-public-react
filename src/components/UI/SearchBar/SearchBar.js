import React from 'react';
import {TextField, SelectField, MenuItem, AutoComplete} from "material-ui";

/**
 * `SelectField` supports a floating label with the `floatingLabelText` property.
 * This can be fixed in place with the `floatingLabelFixed` property,
 * and can be customised with the `floatingLabelText` property.
 */
export const SelectType = props => {
    const items = [
        <MenuItem id='selectAdminList' key={1} value='http://localhost:8081/api/admin/admin/search'
                  primaryText="Administratorių sąrašas"/>,
        <MenuItem id='selectDoctorList' key={2} value='http://localhost:8081/api/admin/doctor/search'
                  primaryText="Gydytojų sąrašas"/>,
        <MenuItem id='selectPatientList' key={3} value='http://localhost:8081/api/admin/patient/search'
                  primaryText="Pacientų sąrašas"/>,
        <MenuItem id='selectPharmacistList' key={4} value='http://localhost:8081/api/admin/pharmacist/search'
                  primaryText="Vaistininkų sąrašas"/>,
    ];
    return (
        <SelectField
            id='selectUserList'
            defaultValue={1}
            value={props.userType}
            onChange={props.handleChange}
            floatingLabelText="Pasirinkite vartotojų sąrašą"
        >
            {items}
        </SelectField>
    )
};

const style = {
    width: '190px',
    textAlign: 'center',
    appearance: 'none'
};

export const SearchFilterByFirstName = props => {
    return (
        <TextField
            style={style}
            id="searchByFirstName"
            hintText="Vartotojo vardas"
            floatingLabelText="Vardas"
            value={props.searchValue}
            onChange={props.handleChange}
        />
    );
};

export const SearchFilterByLastName = props => {
    return (
        <TextField
            style={style}
            id="searchByLastName"
            hintText="Vartotojo pavardė"
            floatingLabelText="Pavardė"
            value={props.searchValue}
            onChange={props.handleChange}
        />
    );
};

export const SearchFilterByPid = props => {
    return (
        <TextField
            style={style}
            id="searchByPid"
            maxLength='11'
            hintText="Vartotojo asmens kodas"
            floatingLabelText="Asmens kodas"
            value={props.searchValue}
            onChange={props.handleChange}
        />
    );
};

export const SelectPageSize = props => {
    const items = [
        <MenuItem id='selectPageSize5' key={1} value='5' primaryText="5"/>,
        <MenuItem id='selectPageSize10' key={2} value='10' primaryText="10"/>,
        <MenuItem id='selectPageSize20' key={3} value='20' primaryText="20"/>,
        <MenuItem id='selectPageSize50' key={4} value='50' primaryText="50"/>,
    ];
    return (
        <SelectField
            id='selectPageSize'
            style={{width: '68px'}}
            value={props.pageSize}
            onChange={props.handleChange}
            floatingLabelText="Kiekis"
        >
            {items}
        </SelectField>
    )
};

export const SearchFilterByDiagnosis = props => (
    <AutoComplete
        textFieldStyle={{style/*, top: '-15px'*/}}
        id="searchByDiagnosis"
        hintText="Diagnozės kodas"
        floatingLabelText="Diagnozė"
        filter={AutoComplete.caseInsensitiveFilter}
        maxSearchResults={5}
        dataSource={props.diagnoses}
        value={props.searchValue}
        onUpdateInput={props.handleChange}
    />
);
