import React from 'react';
import '../../Modal/CreatedUser/CreatedUser.css';

const CreatedPrescriptionFill = (props) => {
    return (
        <div className="creationModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title">Panaudojimas užfiksuotas</h4>
                    </div>
                    <div className="modal-body" style={{ textAlign: 'left' }}>
                        <p>Receptas išrašytas: <strong>{props.prescribedOn}</strong></p>
                        <p>Galioja iki: <strong>{props.validUntil}</strong></p>
                        <p>Veiklioji medžiaga: <strong>{props.activeIngredient}</strong></p>
                        <p>Dozė: <strong>{props.activeIngredientPerDose}{' '}{props.activeIngredientUnits}</strong></p>
                        <p>Panaudojimų skaičius: <strong>{props.fillNum}</strong></p>
                        <p>Paskutinis panaudojimas: <strong>{props.lastFill}</strong></p>
                    </div>
                    <div className="modal-footer">
                        <button onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>

                    </div>
                </div>
            </div>
        </div>
    )
};

export default CreatedPrescriptionFill;