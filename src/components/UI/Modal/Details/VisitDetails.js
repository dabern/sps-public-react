import React from 'react';
// import Aux from '../../../../hoc/Aux';
// import Backdrop from '../../Backdrop/Backdrop'
import '../../Modal/CreatedUser/CreatedUser.css'
// hooray, wasted plenty of time only to find out that react-bootstrap Modal does not work on React16
// import { Modal } from "react-bootstrap";

const VisitDetails = (props) => {
    return (
        <div className="creationModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Informacija apie vizitą</h5>
                    </div>
                    <div className="modal-body" style={{textAlign: 'left'}}>
                        <p>Data: <strong>{props.date}</strong></p>
                        {/* this is obviously stupid, will have to fetch full name: */}
                        <p>Gydytojas: <strong>{props.doctorFirstName}</strong></p>
                        <p>Ligos kodas: <strong>{props.diagnosisId}</strong></p>
                        <p>Pastabos: <strong>{props.notes}</strong></p>
                        <p>Vizito pobūdis: <strong>{props.visitType}</strong></p>
                        <p>Vizito trukmė: <strong>{props.appointmentLength} min</strong></p>
                    </div>
                    <div className="modal-footer">
                        <button onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
                    </div>
                </div>
            </div>
        </div>)
};

export default VisitDetails;

// const VisitDetails = (props) => {
//     return (
//         <div>
//             <Modal show={props.show} hide={props.hide}>
//                 <Modal.Dialog>
//                     <Modal.Header>
//                         <Modal.Title>Modal title</Modal.Title>
//                     </Modal.Header>

//                     <Modal.Body>{props.children}</Modal.Body>

//                     <Modal.Footer>

//                     </Modal.Footer>
//                 </Modal.Dialog>
//             </Modal>
//         </div>
//     );
// }

// export default VisitDetails;
