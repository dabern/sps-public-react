import React from 'react';
import '../../Modal/CreatedUser/CreatedUser.css';

const PrescriptionDetails = (props) => {
        return (
            <div className="creationModal">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Informacija apie receptą</h5>
                        </div>
                        <div className="modal-body" style={{textAlign: 'left'}}>
                        <p>Išrašytas: <strong>{props.prescribedOn}</strong></p>
                        <p>Galioja iki: <strong>{props.validUntil}</strong></p>
                        <p>Veiklioji medžiaga: <strong>{props.activeIngredient}</strong></p>
                        <p>Dozė: <strong>{props.activeIngredientPerDose}{' '}{props.activeIngredientUnits}</strong></p>
                        <p>Vartojimo aprašymas: <strong>{props.dosageNotes}</strong></p>
                        <p>Panaudota kartų: <strong></strong></p>
                        </div>
                        <div className="modal-footer">
                        <button onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>

                        </div>
                    </div>
                </div>
            </div> )
    };

export default PrescriptionDetails;
