import React from 'react';
import '../../Modal/CreatedUser/CreatedUser.css'

const CreatedPrescription = (props) => {
        return (
            <div className="creationModal">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Išrašytas naujas receptas:</h5>
                        </div>
                        <div className="modal-body" style={{textAlign: 'left'}}>
                            <p>Pacientas: <strong>{props.firstName}{' '}{props.lastName}</strong></p>
                            <p>Paciento asmens kodas: <strong>{props.pid}</strong></p>
                            <p>Veiklioji medžiaga: <strong>{props.activeIngredient}</strong></p>
                            <p>Veikliosios medžiagos kiekis dozėje: <strong>{props.activeIngredientPerDose}{' '}{props.activeIngredientUnits}</strong></p>
                            <p>Recepto išrašymo data: <strong>{props.prescribedOn}</strong></p>
                            <p>Receptas galioja iki: <strong>{props.validUntil}</strong></p>
                            <p>Vaisto vartojimo aprašymas: <strong>{props.dosageNotes}</strong></p>
                            <p>{props.children}</p>
                        </div>
                        <div className="modal-footer">
                            <button id='createdPrescriptionBackButton' onClick={props.back} type="button" className="btn btn-primary">Grįžti</button>
                            <button id='createdPrescriptionCloseButton' onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
                        </div>
                    </div>
                </div>
            </div> )
    };

export default CreatedPrescription;
