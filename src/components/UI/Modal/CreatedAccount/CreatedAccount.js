import React from 'react';
const CreatedAccount = (props) => {
    return (
        <div className="creationModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Vartotojo sėkmingai priskirta asmeninė paskyra</h5>
                        <button onClick={props.hide} id='createdUserCloseButton' className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <img style={{width: '80px'}} src={props.image} alt='profile_image'/>
                        <p>Vardas: <strong>{props.firstName}</strong></p>
                        <p>Pavardė: <strong>{props.lastName}</strong></p>
                        <p>Asmens kodas: <strong>{props.pid}</strong></p>
                        <p>Vartotojo vardas: <strong>{props.username}</strong></p>
                        <div>{props.children}</div>
                    </div>
                    <div className="modal-footer">
                        <button id='createdUserBackButton' onClick={props.back} type="button" className="btn btn-secondary" data-dismiss="modal">Grįžti į sąrašą</button>
                    </div>
                </div>
            </div>
        </div> )
};

export default CreatedAccount;
