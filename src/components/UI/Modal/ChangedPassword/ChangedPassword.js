import React from 'react';

const changedPassword = (props) => {
    return (
        <div className="creationModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Slaptažodis sėkmingai pakeistas</h5>
                        <button onClick={props.hide} type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Slaptažodis sėkmingai pakeistas</p>
                    </div>
                    <div className="modal-footer">
                        <button onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
                    </div>
                </div>
            </div>
        </div> )
};

export default changedPassword;
