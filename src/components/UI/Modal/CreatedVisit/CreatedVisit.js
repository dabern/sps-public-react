import React from 'react';
import '../../Modal/CreatedUser/CreatedUser.css'

const CreatedVisit = (props) => {
        return (
            <div className="creationModal">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Sukūrtas naujas ligos įrašas</h5>
                        </div>
                        <div className="modal-body" style={{textAlign: 'left'}}>
                            <p>Vizito data: <strong>{props.visitDate}</strong></p>
                            <p>Paciento vardas, pavardė: <strong>{props.firstName}{' '}{props.lastName}</strong></p>
                            <p>Paciento asmens kodas: <strong>{props.pid}</strong></p>
                            <p>Diagnozė: <strong>{props.diagnosis}</strong></p>
                            <p>Vizito trukmė: <strong>{props.visitLength} <span>min.</span></strong></p>
                            <p>Vizito pastabos: <strong>{props.notes}</strong></p>
                            <div>{props.children}</div>
                        </div>
                        <div className="modal-footer">
                            <button id='createdVisitBackButton' onClick={props.back} type="button" className="btn btn-primary">Grįžti</button>
                            <button id='createdVisitCloseButton' onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
                        </div>
                    </div>
                </div>
            </div> )
    };

export default CreatedVisit;
