import React, {PureComponent} from 'react';
import User from "./User";
import {Table} from "react-bootstrap";
import './DoctorUserList.css';

class UserList extends PureComponent {

    componentWillMount() {
        console.log('DoctorUserList WillMount')
    }

    componentDidMount() {
        console.log('DoctorUserList DidMount')
    }

    render() {

        console.log('DoctorUserList Render');

        const users = this.props.userList;
        const listItems = users.map((user, index) =>
            <User
                key={user.pid}
                firstName={user.firstName}
                lastName={user.lastName}
                pid={user.pid}
                userDetails={() => this.props.userDetails(index)}
                userVisit={() => this.props.userVisit(index)}
                userPrescription={() => this.props.userPrescription(index)}
            />
        );
        return (
            <div id='userList'>
                <Table striped condensed hover responsive>
                    <thead id='userListHeader'>
                    <tr>
                        {/*<th>ID</th>*/}
                        <th id='userPidHeader'>Asmens kodas</th>
                        <th id='userFirstNameHeader'>Vardas</th>
                        <th id='userLastNameHeader'>Pavardė</th>
                        {/*<th id='userDetailsHeader'>Detaliau</th>*/}
                        <th id='userVisitHeader'>Naujas vizitas</th>
                        <th id='userPrescriptionHeader'>Naujas receptas</th>
                    </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}

export default UserList;
