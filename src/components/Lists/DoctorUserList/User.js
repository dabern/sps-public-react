import React from 'react';
import {Button} from "react-bootstrap";
import './DoctorUserList.css';

const user = props => (
    <tr id='user'>
        {/*<td>{props.id}</td>*/}
        <td id='userPidLink' onClick={props.userDetails}>{props.pid}</td>
        <td id='userFirstName'>{props.firstName}</td>
        <td id='userLastName'>{props.lastName}</td>
        {/*<td><Button id="buttonShow" bsStyle="success" onClick={props.userDetails}>Detaliau</Button></td>*/}
        <td><Button id="buttonVisit" bsStyle="success" onClick={props.userVisit}>Vizitas</Button></td>
        <td><Button id="buttonPrescription" bsStyle="warning" onClick={props.userPrescription}>Receptas</Button></td>
    </tr>
);

export default user;
