import React, {PureComponent} from 'react';
import User from "./User";
import {Table} from "react-bootstrap";
import './UserList.css';

// const UserList = (props) => {

class UserList extends PureComponent {

    componentWillMount() {
        console.log('UserList WillMount')
    }

    // shouldComponentUpdate(nextProps) {
    //     console.log('userList shouldUpdate', nextProps.userList !== this.props.userList);
    //     return nextProps.userList !== this.props.userList;
    // }

    componentDidMount() {
        console.log('UserList DidMount')
    }

    render() {

        console.log('UserList Render');

        const users = this.props.userList;
        const listItems = users.map((user, index) =>
            <User
                // id={user.id}
                key={user.pid}
                pid={user.pid}
                firstName={user.firstName}
                lastName={user.lastName}
                userDetails={() => this.props.userDetails(index)}
                userEdit={() => this.props.userEdit(index)}
                userAccount={() => this.props.userAccount(index)}
                patient={this.props.patient}
                hasAccount={user.hasAccount}
            />
        );
        return (
            <div id='userList'>
                <Table striped condensed hover responsive>
                    <thead id='userListHeader'>
                    <tr>
                        {/*<th>ID</th>*/}
                        <th id='userPidHeader'>Asmens kodas</th>
                        <th id='userFirstNameHeader'>Vardas</th>
                        <th id='userLastNameHeader'>Pavardė</th>
                        {/*<th id='userDetailsHeader'>Detaliau</th>*/}
                        <th id='userEditHeader'>Redaguoti vartotoją</th>
                        {this.props.patient ? <th id='userAccountHeader'>Pridėti paskyrą</th> : null}

                    </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}


export default UserList;
