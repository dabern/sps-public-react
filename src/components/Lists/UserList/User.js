import React from 'react';
import {Button} from "react-bootstrap";

const User = (props) => (
    <tr id='user'>
        {/*<td>{props.id}</td>*/}
        <td id='userPid'>{props.pid}</td>
        <td id='userFirstName'>{props.firstName}</td>
        <td id='userLastName'>{props.lastName}</td>
        {/*<td><Button id="buttonShow" bsStyle="success" onClick={props.userDetails}>Detaliau</Button></td>*/}
        <td><Button id="buttonEdit" bsStyle="warning" onClick={props.userEdit}>Redaguoti</Button></td>
        {props.patient ? <td><Button id="buttonAccount" bsStyle="success" disabled={props.hasAccount} onClick={props.userAccount}>{props.hasAccount ? 'Aktyvuotas' : 'Pridėti'}</Button></td> : null}
    </tr>
);

export default User;
