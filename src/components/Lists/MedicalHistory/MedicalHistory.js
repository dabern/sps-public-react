import React, { Component } from 'react';
import { Table } from "react-bootstrap";
// import moment from 'moment';

import Visit from './Visit';

class MedicalHistory extends Component {

    render() {
        const visits = this.props.visitList;
        const listItems = visits.map((visit, index) =>
            <Visit
                date={visit.date}
                key={visit.date + visit.diagnosisId + Math.floor(Math.random() * 10)}
                diagnosisId={visit.diagnosisId}
                doctorFirstName={visit.doctorId}
                onClick={() => this.props.visitDetails(index)}
            />
        );
        return (
            <div id="medicalHistoryList">
                <Table striped condensed hover responsive>
                    <thead id='medicalHistoryListHeader'>
                        <tr>
                            <th id="visitDateHeader">Vizito data</th>
                            <th id="diagnosisIdHeader">Ligos kodas</th>
                            <th id="doctorFullNameHeader">Gydytojas</th>
                            <th id="visitButtonHeader"></th>
                        </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}

export default MedicalHistory;
