import React from 'react';
import {Button} from "react-bootstrap";

const Visit = (props) => (
        <tr id='visit'>
            <td id='visitDate'>{props.date}</td>
            <td id='diagnosisId'>{props.diagnosisId}</td>
            <td id='doctorFullName'>{props.doctorFirstName}</td>
            <td><Button id="buttonShow" bsStyle="success" onClick={props.onClick}>Detaliau</Button></td>
        </tr>
    );

export default Visit;