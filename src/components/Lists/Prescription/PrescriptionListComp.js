import React, {PureComponent} from 'react';
import {Table} from "react-bootstrap";

import Prescription from "./Prescription";

class PrescriptionList extends PureComponent {

    render() {

        const prescriptions = this.props.prescriptionList;

        const listItems = prescriptions.map((prescription, index) =>

            <Prescription
                key={prescription.prescriptionId}
                validUntil={prescription.validUntil}
                prescribedOn={prescription.prescribedOn}
                activeIngredient={prescription.activeIngredient}
                onClick={() => this.props.prescriptionAction(index)}
                buttonTitle={this.props.buttonTitle}
            />
        );
        return (
            <div id='prescriptionList'>
                <Table striped condensed hover responsive>
                    <thead id='prescriptionListHeader'>
                    <tr>
                        <th id='prescriptionValidUntilHeader'>Galioja iki:</th>
                        <th id='prescriptionPrescribedOnHeader'>Išrašytas:</th>
                        <th id='prescriptionActiveIngredientHeader'>Veiklioji medžiaga</th>
                        <th id='prescriptionButtonHeader'></th>
                    </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}

export default PrescriptionList;
