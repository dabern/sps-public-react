import React from 'react';
import {Button} from "react-bootstrap";

const Prescription = (props) => (
    <tr id='prescription'>
        <td id='validUntil'>{props.validUntil}</td>
        <td id='prescribedOn'>{props.prescribedOn}</td>
        <td id='activeIngredient'>{props.activeIngredient}</td>
        <td id='prescriptionButton'><Button id="buttonShow" bsStyle="success" onClick={props.onClick}>{props.buttonTitle}</Button></td>
    </tr>
);

export default Prescription;
