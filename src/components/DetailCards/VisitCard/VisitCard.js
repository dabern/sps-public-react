import React from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import Aux from "../../../hoc/Aux";
import './VisitCard.css';

const VisitCard = (props) => {
    return (
        <Aux>
            <Grid>
                <Row className="VisitCard">
                    <Col md={12}>
                        <div>
                            <strong><span>{props.firstName}</span>
                            {' '}
                            <span>{props.lastName}</span></strong>
                        </div>
                        <div>{props.pid}</div>
                        <div>{props.children}</div>
                    </Col>
                </Row>
            </Grid>
        </Aux>

    )
};

export default VisitCard;