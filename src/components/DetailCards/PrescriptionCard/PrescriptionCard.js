import React from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import Aux from "../../../hoc/Aux";
import './PrescriptionCard.css';

const PrescriptionCard = (props) => {
    return (
        <Aux>
            <Grid>
                <Row className="PrescriptionCard">
                    <Col md={12}>
                        <div>
                        <p>Išrašytas: <strong></strong>{props.prescribedOn}</p>
                        <p>Galioja iki: <strong>{props.validUntil}</strong></p>
                        <p>Veiklioji medžiaga: <strong>{props.activeIngredient}</strong></p>
                        <p>Dozė: <strong>{props.activeIngredientPerDose}{' '}{props.activeIngredientUnits}</strong></p>
                        <p>Vartojimo aprašymas: <strong>{props.dosageNotes}</strong></p>
                        </div>
                    </Col>
                </Row>
            </Grid>
        </Aux>

    )
};

export default PrescriptionCard;