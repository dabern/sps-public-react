import React, { Component } from 'react';
import {NavLink, withRouter} from "react-router-dom";
import jwtDecode from 'jwt-decode';
import { connect } from "react-redux";
import {List, ListItem} from 'material-ui';
import PersonAdd from 'material-ui/svg-icons/social/person-add';

import './UserNavigation.css';

class UserNavigation extends Component {
    render() {
        let navigation = null;

        if (this.props.isAuthenticatedRedux) {
            const token = this.props.isAuthenticatedRedux;
            const decodedToken = jwtDecode(token);
            // console.log(decodedToken);
            const roles = decodedToken.authorities;

            if (roles.includes("ROLE_PATIENT")) {
                navigation =
                    <div id='patientSidebarNavigation' className="UserButtons">
                        <NavLink id='patientSidebarHome' to="/patient/home" className="NavLink">Pagrindinis</NavLink>
                        <NavLink id='patientSidebarProfile' to="/patient/profile" className="NavLink">Mano profilis</NavLink>
                    </div>
            }
            else if (roles.includes("ROLE_DOCTOR")) {
                navigation =
                    <div id='doctorSidebarNavigation' className="UserButtons">
                        <NavLink id='doctorSidebarHome' to="/doctor/home" className="NavLink">Pagrindinis</NavLink>
                        <NavLink id='doctorSidebarProfile' to="/doctor/profile" className="NavLink">Mano profilis</NavLink>
                        <NavLink id='doctorSidebarPrescription' to="/doctor/newprescription" className="NavLink">Naujas receptas</NavLink>
                        <NavLink id='doctorSidebarMedicalRecord' to="/doctor/newrecord" className="NavLink">Naujas ligos įrašas</NavLink>
                    </div>
            }
            else if (roles.includes("ROLE_PHARMACIST")) {
                navigation = (
                    <div id='pharmacistSidebarNavigation' className="UserButtons">
                        <NavLink id="pharmacistSidebarHome" to="/pharmacist/home" className="NavLink">Pagrindinis</NavLink>
                        <NavLink id='pharmacistSidebarProfile' to="/pharmacist/profile" className="NavLink">Mano profilis</NavLink>
                    </div>)
            } else if (roles.includes("ROLE_ADMIN")) {
                navigation = (
                    <div id='adminSidebarNavigation' className="UserButtons">
                        <NavLink id='adminSidebarProfile' to="/admin/profile" className="NavLink">Mano profilis</NavLink>
                        <NavLink id='adminSidebarUserList' to="/admin/userList" className="NavLink">Vartotojų sąrašas</NavLink>
                        <NavLink id='adminSidebarRegister' to={{}} onClick={this.props.clicked} className="NavLink">Registruoti <span className='caret'/></NavLink>
                        {this.props.show ?
                            <List id='adminSidebarRegisterUsers' style={{textAlign: 'left', paddingLeft: '10%'}}>
                                <ListItem id='adminSidebarRegisterAdmin' onClick={() => this.props.history.push('/admin/register/admin')} primaryText='Administratorių' rightIcon={<PersonAdd/>}/>
                                <ListItem id='adminSidebarRegisterDoctor' onClick={() => this.props.history.push('/admin/register/doctor')} primaryText='Gydytoją' rightIcon={<PersonAdd/>}/>
                                <ListItem id='adminSidebarRegisterPatient' onClick={() => this.props.history.push('/admin/register/patient')} primaryText='Pacientą' rightIcon={<PersonAdd/>}/>
                                <ListItem id='adminSidebarRegisterPharmacist' onClick={() => this.props.history.push('/admin/register/pharmacist')} primaryText='Vaistininką' rightIcon={<PersonAdd/>}/>
                            </List>
                         :null}
                    </div>
                )
            }
        }

        return <div id='sidebarNavigation'>{navigation}</div>;

    }
}

// Redux states
const mapStateToProps = state => {
    return {
        isAuthenticatedRedux: state.auth.accessToken
    }
};

export default withRouter(connect(mapStateToProps)(UserNavigation));
