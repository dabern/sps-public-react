import React, { Component } from 'react';

import UserNavigation from "./UserNavigation";

class UserLayout extends Component {

    render() {
        return (
            <div>
                <UserNavigation show={this.props.show} clicked={this.props.clicked}/>
                <main className="UserLayout">
                    {this.props.children}
                </main>
            </div>
        )
    }
}

export default UserLayout;
