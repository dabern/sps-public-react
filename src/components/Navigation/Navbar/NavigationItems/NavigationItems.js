import React from 'react';
import './NavigationItems.css';
import RouteNavItem from './RouteNavItem/RouteNavItem';

const navigationItems = (props) => (
    <div>
        <ul id='navigationItems' className="NavigationItems">
            {!props.isAuthenticated ?
                <RouteNavItem id='navigationItemLogin' to="/login">Prisijungti</RouteNavItem> :
                <RouteNavItem id='navigationItemLogout' to="/logout">Atsijungti</RouteNavItem>
            }
            <RouteNavItem id='navigationItemStatistics' to="/statistics">Statistika</RouteNavItem>
            {/*<RouteNavItem to="/admin/home">Admin</RouteNavItem>*/}
            {/*<RouteNavItem to="/doctor/home">Gydytojo main page</RouteNavItem>*/}
            {/*<RouteNavItem to="/patient/profile">Paciento profilis</RouteNavItem>*/}
            {/*<RouteNavItem to="/pharmacist/profile">Vaistininko profilis</RouteNavItem>*/}
        </ul>
    </div>

);

export default navigationItems;
