import React, { Component } from "react";
import {withRouter} from "react-router-dom";
import "./App.css";
import Routes from "./Routes";
import Navbar from "./components/Navigation/Navbar/Navbar";
import {connect} from "react-redux";
import * as actions from './store/actions/authentication'

class App extends Component {

    componentWillMount() {
        this.props.onTryAutoLogin()
    }

    render() {
        return (
            <div>
                <Navbar isAuthenticated={this.props.isAuthenticatedRedux} />
                <Routes />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticatedRedux: state.auth.accessToken
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoLogin: () => dispatch(actions.authCheckState())
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
