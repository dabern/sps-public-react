import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
//import registerServiceWorker from "./registerServiceWorker";
import "./index.css";
import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {reducer} from 'redux-form'
import authReducer from './store/reducers/authentication';
import userCreationReducer from './store/reducers/userCreation'
import getMuiTheme from "material-ui/styles/getMuiTheme";
import {MuiThemeProvider} from "material-ui";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import userListReducer from "./store/reducers/userList";
import userByIdReducer from "./store/reducers/user";
import specializationCreationReducer from './store/reducers/specializationCreation';
import prescriptionListReducer from "./store/reducers/prescriptionList";
import visitListReducer from "./store/reducers/medicalHistory";
import otherListReducer from "./store/reducers/otherList";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    form: reducer,
    auth: authReducer,
    create: userCreationReducer,
    getUser: userByIdReducer,
    fetch: userListReducer,
    specialization: specializationCreationReducer,
    fetchVisits: visitListReducer,
    fetchPrescriptions: prescriptionListReducer,
    fetchOther: otherListReducer
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const app = (
    <Provider store={store}>
        <Router>
            <MuiThemeProvider muiTheme={getMuiTheme()}>
            <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);

ReactDOM.render(
 app,
  document.getElementById("root")
);

if (module.hot) {
    module.hot.accept();
    }
// registerServiceWorker();
