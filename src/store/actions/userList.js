import * as actionTypes from './actionTypes'
import axios from 'axios'

////////////////////////////////////////////////////
///               fetchUsers                     ///
////////////////////////////////////////////////////
export const fetchUsersSuccess = (userList,totalPages, totalElements, pageSize, pageNumber) => {   ///
    return {                                     ///
        type: actionTypes.FETCH_USERS_SUCCESS,   ///
        userList: userList,
        totalPages: totalPages,
        totalElements: totalElements,
        pageSize: pageSize,
        pageNumber: pageNumber
    }                                            ///
};                                               ///
export const fetchUsersFail = error => {         ///
    return {                                     ///
        type: actionTypes.FETCH_USERS_FAIL,      ///
        error: error                             ///
    }                                            ///
};                                               ///
export const fetchUsersStart = () => {           ///
    return {                                     ///
        type: actionTypes.FETCH_USERS_START      ///
    }                                            ///
};                                               ///

export const fetchUsersInit = () => {
    return {
        type: actionTypes.FETCH_USERS_INIT
    }
};
////////////////////////////////////////////////////

// Fetch users initially with loading state
export const fetchUsers = (url, token, pageSize, searchParams) => {
    return dispatch => {
        dispatch(fetchUsersStart()); // only difference vs changePageFetchUsers
        if (token) {
            console.log(searchParams);
            const sort = '?sort=lastName%2Casc&sort=firstName%2Casc';
            console.log(sort);
            axios.post(url + sort, searchParams, {headers: {'Authorization': 'Bearer ' + token}, params:{'size': pageSize}})
                .then(response => {
                    const getUsers = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    console.log('userList reducer fetchUsers response ',response);
                    dispatch(fetchUsersSuccess(getUsers, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                console.log('userList reducer fetchUsers error ',error);
                dispatch(fetchUsersFail(error.message))
            })
        } else {
            dispatch(fetchUsersFail("You are not logged in!"))
        }
    }
};
// Fetch users by changing pages, there's no loading state, user unfriendly interface with loading
export const changePageFetchUsers = (url, token, pageNumber, pageSize, searchParams) => {
    return dispatch => {
        if (token) {
            const sort = '?sort=lastName%2Casc&sort=firstName%2Casc';
            axios.post(url + sort, searchParams, {headers: {'Authorization': 'Bearer ' + token}, params:{'page': pageNumber, 'size': pageSize}})
                .then(response => {
                    const getUsers = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    console.log('userList reducer changePageFetchUsers response ',response);
                    dispatch(fetchUsersSuccess(getUsers, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                console.log('userList reducer changePageFetchUsers error ',error);
                dispatch(fetchUsersFail(error.message))
            })
        } else {
            dispatch(fetchUsersFail("You are not logged in!"))
        }
    }
};

// //Fetch data using get
// export const fetchVisits = (url, token, pageSize) => {
//     return dispatch => {
//         dispatch(fetchUsersStart()); // only difference vs changePageFetchUsers
//         if (token) {
//             axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params:{'size': pageSize}})
//                 .then(response => {
//                     const getUsers = response.data.content;
//                     const totalPages = response.data.totalPages;
//                     const totalElements = response.data.totalElements; //total count of objects
//                     const pageSize = response.data.size; // page size
//                     const pageNumber = response.data.number; // page number
//                     console.log('userList reducer fetchData response ',response);
//                     dispatch(fetchUsersSuccess(getUsers, totalPages, totalElements, pageSize, pageNumber));
//                 }).catch(error => {
//                 console.log('userList reducer fetchData error ',error);
//                 dispatch(fetchUsersFail(error.message))
//             })
//         } else {
//             dispatch(fetchUsersFail("You are not logged in!"))
//         }
//     }
// };

// export const fetchUsersById = (url, token, pageSize) => {
//     return dispatch => {
//         dispatch(fetchUsersStart()); // only difference vs changePageFetchUsers
//         if (token) {
//             axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params:{'size': pageSize}})
//                 .then(response => {
//                     const getUsers = response.data.content;
//                     const totalPages = response.data.totalPages;
//                     const totalElements = response.data.totalElements; //total count of objects
//                     const pageSize = response.data.size; // page size
//                     const pageNumber = response.data.number; // page number
//                     console.log(totalPages);
//                     console.log(response);
//                     dispatch(fetchUsersSuccess(getUsers, totalPages, totalElements, pageSize, pageNumber));
//                 }).catch(error => {
//                 console.log(error);
//                 dispatch(fetchUsersFail(error.message))
//             })
//         } else {
//             dispatch(fetchUsersFail("You are not logged in!"))
//         }
//     }
// };
