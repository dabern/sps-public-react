import * as actionTypes from './actionTypes';
import axios from "axios/index";
import {userType} from '../../shared/utility';
import jwtDecode from "jwt-decode";

export const authInit = () => {
    return {
        type: actionTypes.AUTH_INIT
    }
};

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
};

export const authSuccess = (access_token) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        access_token: access_token,
    }
};

export const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
};

export const authUserAccount = authUserAccount => {
    return {
        type: actionTypes.AUTH_USER_ACCOUNT,
        authUser: authUserAccount
    }
};

// export const authUserDetails = authUserDetails => {
//     return {
//         type: actionTypes.AUTH_USER_DETAILS,
//         authUserDetails: authUserDetails
//     }
// };

export const authLogout = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('scope');
    localStorage.removeItem('expirationDate');
    return {
        type: actionTypes.AUTH_LOGOUT,
    }
};

export const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(authLogout());
        }, expirationTime * 1000);
    };
};

export const authUserAccountInfo = token => {
    const decodedToken = jwtDecode(token);
    const username = decodedToken.user_name;
    return dispatch => {
        axios.get('http://localhost:8081/api/me', {headers: {'Authorization': 'Bearer ' + token}})
            .then(response => {
                dispatch(authUserAccount({...response.data, username: username}))
            }).catch(error => {
            if(typeof error.response === 'undefined'){
                dispatch(authFail(error.message));
            } else {
                dispatch(authFail(error.response.data.message))
            }
        })
    }
};

// export const authUserInfo = (token, userType, userId)=> {
//     return dispatch => {
//         axios.get('http://localhost:8081/api/admin/'+ userType + '/' + userId, {headers: {'Authorization': 'Bearer ' + token}})
//             .then(response => {
//                 dispatch(authUserDetails(response.data))
//             }).catch(error => {
//             if(typeof error.response === 'undefined'){
//                 dispatch(authFail(error.message));
//             } else {
//                 dispatch(authFail(error.response.data.message))
//             }
//         })
//     }
// };

export const authCheckState = () => {
    return dispatch => {
        const access_token = localStorage.getItem('access_token');
        if (!access_token) {
            dispatch(authLogout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate < new Date()) {
                dispatch(authLogout());
            } else {
                // const decodedToken = jwtDecode(access_token);
                // const roles = decodedToken.authorities;
                // const type = userType(roles);
                // const userId = decodedToken.id;
                // dispatch(authUserInfo(access_token, type, userId));
                dispatch(authUserAccountInfo(access_token));
                dispatch(authSuccess(access_token));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    };
};

// export const setAuthRedirectPath = (path) => {
//     return {
//         type: actionTypes.SET_AUTH_REDIRECT_PATH,
//         path: path
//     };
// };

export const auth = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            grant_type: 'password',
            username: username,
            password: password,
        };
        const authHeader = btoa('trusted-app:secret');
        const headers = {'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic ' + authHeader};
        const tokenUrl = 'http://localhost:8081/oauth/token';
        // const formData = 'grant_type=password' + '&username=' + username + '&password=' + password;
        const objectToString = JSON.stringify(authData);
        const formData = objectToString.replace(/[{} "]/g,'').replace(/:/g,'=').replace(/,/g,'&');
        console.log(formData);
        axios.post(tokenUrl, formData, {headers: headers})
            .then(response => {
                console.log('authentication reducer response ',response.data);
                const expirationDate = new Date(new Date().getTime() + response.data.expires_in * 1000);
                localStorage.setItem('access_token', response.data.access_token);
                localStorage.setItem('expirationDate', expirationDate);
                // localStorage.setItem('refresh_token', response.data.refresh_token);
                // localStorage.setItem('scope', response.data.scope);
                const token = response.data.access_token;
                // dispatch(authUserInfo(token, type, userId));
                dispatch(authSuccess(token));
                dispatch(checkAuthTimeout(response.data.expires_in));
                dispatch(authUserAccountInfo(token))
            })
            .catch(error => {
                if(typeof error.response === 'undefined'){
                    dispatch(authFail('Tinklo klaida'));
                } else {
                    dispatch(authFail('Neteisingi prisijungimo duomenys'));
                    console.log(error)
                }
            });
    };
};
