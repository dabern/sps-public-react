import * as actionTypes from './actionTypes'
import axios from 'axios'

////////////////////////////////////////////////////
///               fetchVisits                     ///
////////////////////////////////////////////////////
export const fetchVisitsSuccess = (visitList,totalPages, totalElements, pageSize, pageNumber) => {   ///
    return {                                     ///
        type: actionTypes.FETCH_VISITS_SUCCESS,   ///
        visitList: visitList,
        totalPages: totalPages,
        totalElements: totalElements,
        pageSize: pageSize,
        pageNumber: pageNumber
    }                                            ///
};                                               ///
export const fetchVisitsFail = error => {         ///
    return {                                     ///
        type: actionTypes.FETCH_VISITS_FAIL,      ///
        error: error                             ///
    }                                            ///
};                                               ///
export const fetchVisitsStart = () => {           ///
    return {                                     ///
        type: actionTypes.FETCH_VISITS_START      ///
    }                                            ///
};                                               ///

export const fetchVisitsInit = () => {
    return {
        type: actionTypes.FETCH_VISITS_INIT
    }
};
////////////////////////////////////////////////////

//Fetch visits using get
export const fetchVisits = (url, token, pageSize) => {
    return dispatch => {
        dispatch(fetchVisitsStart());
        if (token) {
            axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params:{'size': pageSize}})
                .then(response => {
                    const getVisits = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    console.log('visitList reducer fetchData response ',response);
                    dispatch(fetchVisitsSuccess(getVisits, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                console.log('visitList reducer fetchData error ',error);
                dispatch(fetchVisitsFail(error.message))
            })
        } else {
            dispatch(fetchVisitsFail("You are not logged in!"))
        }
    }
};

//Fetch visits using get
export const changePageFetchVisits = (url, token, pageNumber, pageSize) => {
    return dispatch => {
        if (token) {
            axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params:{'page': pageNumber, 'size': pageSize}})
                .then(response => {
                    const getVisits = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    console.log('visitList reducer fetchVisits response ',response);
                    dispatch(fetchVisitsSuccess(getVisits, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                console.log('visitList reducer fetchVisits error ',error);
                dispatch(fetchVisitsFail(error.message))
            })
        } else {
            dispatch(fetchVisitsFail("You are not logged in!"))
        }
    }
};
