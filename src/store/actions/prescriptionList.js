import * as actionTypes from './actionTypes'
import axios from 'axios'


              /// fetchPrescriptions

export const fetchPrescriptionsSuccess = (prescriptionList,totalPages, totalElements, pageSize, pageNumber) => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_SUCCESS,
        prescriptionList: prescriptionList,
        totalPages: totalPages,
        totalElements: totalElements,
        pageSize: pageSize,
        pageNumber: pageNumber
    }
};
export const fetchPrescriptionsFail = error => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_FAIL,
        error: error
    }
};
export const fetchPrescriptionsStart = () => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_START
    }
};

export const fetchPrescriptionsInit = () => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_INIT
    }
};


//Fetch prescriptions using get
export const fetchPrescriptions = (url, token, pageSize) => {
    return dispatch => {
        dispatch(fetchPrescriptionsStart());
        if (token) {
            axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params:{'size': pageSize}})
                .then(response => {
                    const getPrescriptions = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    console.log('prescriptionList reducer fetchData response ',response);
                    dispatch(fetchPrescriptionsSuccess(getPrescriptions, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                console.log('prescriptionList reducer fetchData error ',error);
                dispatch(fetchPrescriptionsFail(error.message))
            })
        } else {
            dispatch(fetchPrescriptionsFail("You are not logged in!"))
        }
    }
};

//Fetch prescriptions using get
export const changePageFetchPrescriptions = (url, token, pageNumber, pageSize) => {
    return dispatch => {
        if (token) {
            axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params:{'page': pageNumber, 'size': pageSize}})
                .then(response => {
                    const getPrescriptions = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    console.log('PrescriptionList reducer fetchPrescriptions response ',response);
                    dispatch(fetchPrescriptionsSuccess(getPrescriptions, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                console.log('prescriptionList reducer fetchPrescriptions error ',error);
                dispatch(fetchPrescriptionsFail(error.message))
            })
        } else {
            dispatch(fetchPrescriptionsFail("You are not logged in!"))
        }
    }
};

// //Post a prescription fill using get
// export const postPrescription = (url, token, pageSize) => {
//     return dispatch => {
//         dispatch(postPrescriptionStart());
//         if (token) {
//             axios.post(url, params, {headers: {'Authorization': 'Bearer ' + token}, params:{'size': pageSize}})
//                 .then(response => {
//                     const getUsers = response.data.content;
//                     const totalPages = response.data.totalPages;
//                     const totalElements = response.data.totalElements; //total count of objects
//                     const pageSize = response.data.size; // page size
//                     const pageNumber = response.data.number; // page number
//                     console.log('userList reducer fetchUsers response ',response);
//                     dispatch(postPrescriptionSuccess(getUsers, totalPages, totalElements, pageSize, pageNumber));
//                 }).catch(error => {
//                 console.log('userList reducer fetchUsers error ',error);
//                 dispatch(postPrescriptionFail(error.message))
//             })
//         } else {
//             dispatch(postPrescriptionFail("You are not logged in!"))
//         }
//     }
// };
