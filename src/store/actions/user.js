import * as actionTypes from './actionTypes'
import axios from 'axios'

////////////////////////////////////////////////////
///               fetchUser                      ///
////////////////////////////////////////////////////
export const fetchUserSuccess = (userById) => {  ///
    return {                                     ///
        type: actionTypes.FETCH_USER_SUCCESS,    ///
        userById: userById,                      ///
    }                                            ///
};                                               ///
export const fetchUserFail = error => {          ///
    return {                                     ///
        type: actionTypes.FETCH_USER_FAIL,       ///
        error: error                             ///
    }                                            ///
};                                               ///
export const fetchUserStart = () => {            ///
    return {                                     ///
        type: actionTypes.FETCH_USER_START       ///
    }                                            ///
};                                               ///
////////////////////////////////////////////////////

export const fetchUser = (url, token) => {
    return dispatch => {
        dispatch(fetchUserStart());
        if (token) {
            axios.get(url, {headers: {'Authorization': 'Bearer ' + token}})
                .then(response => {
                    const getUser = response.data;
                    console.log('user.js fetchUser response ',response);
                    if(getUser.doctorPid) {
                        const getPatientUser = {...getUser, doctorPidNullable: getUser.doctorPid};
                        dispatch(fetchUserSuccess(getPatientUser));
                    }else {
                        dispatch(fetchUserSuccess(getUser));
                    }
                }).catch(error => {
                console.log('user.js fetchUser error ',error);
                dispatch(fetchUserFail(error.message))
            })
        } else {
            dispatch(fetchUserFail("You are not logged in!"))
        }
    }
};
export const fetchUserInit = () => {
    return {
        type: actionTypes.FETCH_USER_INIT
    }
};
