import * as actionTypes from './actionTypes'
import axios from 'axios'
import {fetchUserSuccess} from "./user";

/////////////////////////////////////////////////////////////
// specializationCreation                                           ///
/////////////////////////////////////////////////////////////
export const specializationCreationSuccess = specializationData => {          ///
    return {                                              ///
        type: actionTypes.SPECIALIZATION_CREATION_SUCCESS,          ///
        specializationData: specializationData                                ///
    }                                                     ///
};                                                        ///
export const specializationCreationFail = error => {                ///
    return {                                              ///
        type: actionTypes.SPECIALIZATION_CREATION_FAIL,             ///
        error: error                                      ///
    }                                                     ///
};                                                        ///
export const specializationCreationStart = () => {                  ///
    return {                                              ///
        type: actionTypes.SPECIALIZATION_CREATION_START             ///
    }                                                     ///
};                                                        ///
/////////////////////////////////////////////////////////////

export const specializationCreation = (url, specializationData, token) => {
    return dispatch => {
        dispatch(specializationCreationStart());
        axios.post(url, specializationData, {headers: {'Authorization': 'Bearer ' + token}})
            .then(response => {
                console.log('specializationCreation reducer specializationCreation response ',response);
                dispatch(specializationCreationSuccess(specializationData));
                const specId = response.data.id;
                dispatch(fetchUserSuccess({specializationID: specId}))
            })
            .catch(error => {
                if(error.response){
                    console.log('specializationCreation reducer specializationCreation error ',error);
                    dispatch(specializationCreationFail('Tokia specializacija jau yra'))
                }else {
                    dispatch(specializationCreationFail('Tinklo klaida'))
                }

            });
    }
};

export const specializationUpdate = (url, specializationData, token) => {
    return dispatch => {
        dispatch(specializationCreationStart());
        axios.put(url, specializationData, {headers: {'Authorization': 'Bearer ' + token}})
            .then(response => {
                console.log('specializationCreation reducer specializationUpdate response ',response);
                dispatch(specializationCreationSuccess(specializationData));
            })
            .catch(error => {
                if(error.response){
                    console.log('specializationCreation reducer specializationUpdate error ',error);
                    dispatch(specializationCreationFail('Tokia specializacija jau yra'))
                }else {
                    dispatch(specializationCreationFail('Tinklo klaida'))
                }

            });
    }
};

export const creationInit = () => {
    return {
        type: actionTypes.SPECIALIZATION_CREATION_INIT
    }
};
