import * as actionTypes from './actionTypes'
import axios from 'axios'
import {reset} from 'redux-form'

/////////////////////////////////////////////////////////////
// userCreation                                           ///
/////////////////////////////////////////////////////////////
export const userCreationSuccess = userData => {          ///
    return {                                              ///
        type: actionTypes.USER_CREATION_SUCCESS,          ///
        userData: userData                                ///
    }                                                     ///
};                                                        ///
export const userCreationFail = error => {                ///
    return {                                              ///
        type: actionTypes.USER_CREATION_FAIL,             ///
        error: error                                      ///
    }                                                     ///
};                                                        ///
export const userCreationStart = () => {                  ///
    return {                                              ///
        type: actionTypes.USER_CREATION_START             ///
    }                                                     ///
};                                                        ///
/////////////////////////////////////////////////////////////

export const userCreation = (url, userData, token, form, type, accountData) => {
    return dispatch => {
        dispatch(userCreationStart());
        axios.post(url, userData, {headers: {'Authorization': 'Bearer ' + token}, params: accountData})
            .then(response => {
                console.log('userCreation reducer userCreation response ', response);
                dispatch(userCreationSuccess({...userData, id: response.data}));
                dispatch(reset(form));
            })
            .catch(error => {
                if (error.response.data) {
                    console.log('userData entered ', userData);
                    if (error.response.data.message === 'Username[' + userData.username + '] already taken.') {
                        dispatch(userCreationFail('Pasirinktas vartotojo vardas yra užimtas'))
                    } else if (error.response.data.message.includes('with pid ' + userData.pid + ' already exists')) {
                        dispatch(userCreationFail(type + ' su nurodytu asmens kodu jau egzistuoja'))
                    } else {
                        dispatch(userCreationFail(error.response.data.message))
                    }
                } else {
                    console.log('userCreation reducer userUpdate error ', error);
                    dispatch(userCreationFail('Tinklo klaida'))
                }
            });
    }
};

export const userUpdate = (url, userData, token, type) => {
    return dispatch => {
        dispatch(userCreationStart());
        axios.put(url, userData, {headers: {'Authorization': 'Bearer ' + token}})
            .then(response => {
                console.log('userCreation reducer userUpdate response ', response);
                dispatch(userCreationSuccess(userData));
            })
            .catch(error => {
                if (error.response.data) {
                    if (error.response.data.message.includes('with pid ' + userData.pid + ' already exists.')) {
                        dispatch(userCreationFail(type + ' su nurodytu asmens kodu jau egzistuoja'))
                        // dispatch(fetchUserSuccess({...userData, pid: null}))
                    } else {
                        dispatch(userCreationFail(error.response.data.message))
                    }
                } else {
                    console.log('userCreation reducer userUpdate error ', error);
                    dispatch(userCreationFail('Tinklo klaida'))
                }
            });
    }
};

export const creationInit = () => {
    return {
        type: actionTypes.USER_CREATION_INIT
    }
};
