import * as actionTypes from '../actions/actionTypes'

const initialState = {
    prescriptionList: [],
    loading: false,
    error: null,
    totalPages: null,
    totalElements: null,
    pageSize: null,
    pageNumber: 0
};

const prescriptionListReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PRESCRIPTIONS_SUCCESS:
            return {
                ...state, prescriptionList: action.prescriptionList, totalPages: action.totalPages,
                totalElements: action.totalElements, pageSize: action.pageSize,
                pageNumber: action.pageNumber, loading: false, error: null
            };
        case actionTypes.FETCH_PRESCRIPTIONS_FAIL:
            return {...state, loading: false, error: action.error};
        case actionTypes.FETCH_PRESCRIPTIONS_START:
            return {...state, loading: true};
        case actionTypes.FETCH_PRESCRIPTIONS_INIT:
            return {...state, error: null, prescriptionList: null};
        default:
            return state;
    }
};

export default prescriptionListReducer;
