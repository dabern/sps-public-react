import * as actionTypes from '../actions/actionTypes'

const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD:
      return {data: action.data};
    default:
      return state
  }
};

/**
 * Simulates data loaded into this reducer from somewhere
 */
export const dataAction = data => ({type: actionTypes.LOAD, data});

export default reducer

// replaced with user reducer atm(temporary), remove it on next commit or leave it as an example
