import * as actionTypes from '../actions/actionTypes'

const initialState = {
    accessToken: null,
    authUser: null,
    // authUserDetails: null,
    error: null,
    loading: false,
    // authRedirectPath: '/'
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_INIT:
            return {...state, error: null, loading: false};
        case actionTypes.AUTH_START:
            return {...state, loading: true, error: null};
        case actionTypes.AUTH_SUCCESS:
            return {...state, accessToken: action.access_token, error: null, loading: false};
        case actionTypes.AUTH_FAIL:
            return {...state, error: action.error, loading: false};
        case actionTypes.AUTH_LOGOUT:
            return {...state, accessToken: null, authUser: null};
        case actionTypes.AUTH_USER_ACCOUNT:
            return {...state, authUser: action.authUser, loading: false};
        // case actionTypes.AUTH_USER_DETAILS:
        //     return {...state, authUserDetails: action.authUserDetails, loading: false};
        // case actionTypes.SET_AUTH_REDIRECT_PATH:
        //     return {...state, authRedirectPath: action.path};
        default:
            return state;
    }
};

export default authReducer;
