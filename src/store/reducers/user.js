import * as actionTypes from '../actions/actionTypes'

const initialState = {
    userById: null,
    loading: false,
    error: null,
};

const userByIdReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_SUCCESS:
            return {...state, userById: action.userById, loading: false, error: null};
        case actionTypes.FETCH_USER_FAIL:
            return {...state, loading: false, error: action.error};
        case actionTypes.FETCH_USER_START:
            return {...state, loading: true};
        case actionTypes.FETCH_USER_INIT:
            return {...state, error: null, userById: null};
        default:
            return state;
    }
};

export default userByIdReducer;
