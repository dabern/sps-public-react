/* eslint-disable */
import jwtDecode from "jwt-decode";

export const updateObject = (oldObject, updatedProps) => {
    return {
        ...oldObject,
        ...updatedProps
    }
};

export const pidToDateConverter = values => {
    let pidFirst = values[0];
    let dobCentury = null;
    const dobYear = values.slice(1, 3);
    const dobMonth = values.slice(3, 5);
    const dobDay = values.slice(5, 7);

    if (pidFirst === '1' || pidFirst === '2') {
        dobCentury = '18'
    } else if (pidFirst === '3' || pidFirst === '4') {
        dobCentury = '19'
    } else if (pidFirst === '5' || pidFirst === '6') {
        dobCentury = '20'
    } else if (pidFirst === '7' || pidFirst === '8') {
        dobCentury = '21'
    }
    return new Date(dobCentury + dobYear + '-' + dobMonth + '-' + dobDay).toLocaleDateString("lt-LT");
};

export const personalIdLastDigitVerification = values => {
    let lastDigit = null;
    let sum = 0;
    for (let i = 0; i < 10; i++) {
        if (i < 9) {
            sum += (parseInt(values[i], 10) * (i + 1));
        } else {
            sum += (parseInt(values[i], 10) * (1));
        }
    }
    if (sum % 11 !== 10) {
        lastDigit = (sum % 11).toString();
    } else {
        sum = 0;
        for (let i = 0; i < 10; i++) {
            if (i < 7) {
                sum += (parseInt(values[i], 10) * (i + 3));
            } else {
                sum += (parseInt(values[i], 10) * (i - 6));
            }
        }
        if (sum % 11 !== 10) {
            lastDigit = (sum % 11).toString();
        } else {
            lastDigit = '0';
        }
    }
    return lastDigit;
};

export const userType = type => {
    if (type.includes('ROLE_ADMIN')) {
        return 'admin';
    } else if (type.includes('ROLE_DOCTOR')) {
        return 'doctor';
    } else if (type.includes('ROLE_PATIENT')) {
        return 'patient';
    } else if (type.includes('ROLE_PHARMACIST')) {
        return 'pharmacist';
    }
};

export const role = token => {
    const decodedToken = jwtDecode(token);
    const roles = decodedToken.authorities;
    return userType(roles);
};

export const toFirstUpperCaseToLowerCase = value => value && value.replace(/[^a-ząčęėįšųūž„“._\- ]/i, '').charAt(0).toUpperCase() + value.replace(/[^a-ząčęėįšųūž„“._\- ]/i, '').slice(1).toLowerCase();
export const onlyNumbers = value => value && value.replace(/[^\d]/g, '');

export const stringToDate = value => {
    if (!value) {
        return value
    }
    const onlyNums = value.replace(/[^\d]/g, '');
    if (onlyNums.length <= 4) {
        return onlyNums
    }
    if (onlyNums.length <= 6) {
        return `${onlyNums.slice(0, 4)}-${onlyNums.slice(4)}`
    }
    return `${onlyNums.slice(0, 4)}-${onlyNums.slice(4, 6)}-${onlyNums.slice(6, 8)}`
};

export const checkValidity = (value, rules) => {
    let isValid = true;
    if (rules.required && isValid) {
        isValid = value.trim() !== '';
    }
    if (rules.minLength && isValid) {
        isValid = value.length >= rules.minLength;
    }
    if (rules.maxLength && isValid) {
        isValid = value.length <= rules.maxLength;
    }
    if (rules.isEmail && isValid) {
        const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        isValid = pattern.test(value);
    }
    if (rules.isNumber && isValid) {
        const pattern = /^\d+$/;
        isValid = pattern.test(value);
    }
    if (rules.isPid && isValid) {
        const pattern = /^[3-6][0-9]{2}[0,1][0-9][0-3][0-9][0-9]{4}$/;
        isValid = pattern.test(value);
    }
    if (rules.isString && isValid) {
        const pattern = /^[a-zA-Z]+$/;
        isValid = pattern.test(value);
    }
    if (rules.isStringUpperFirst && isValid) {
        const pattern = /^[A-Z][a-zA-Z_ ]+$/;
        isValid = pattern.test(value);
    }
    if (rules.isWorkplace && isValid) {
        const pattern = /^(VšĮ|UAB|AB|MB)[a-zA-Z_ ]+$/;
        isValid = pattern.test(value);
    }
    if (rules.isDate && isValid) {
        // date validation with leap year check (YYYY-MM-DD date format)
        const pattern = /^(((18|19|20)[0-9]{2}[\-.](0[13578]|1[02])[\-.](0[1-9]|[12][0-9]|3[01]))|(18|19|20)[0-9]{2}[\-.](0[469]|11)[\-.](0[1-9]|[12][0-9]|30)|(18|19|20)[0-9]{2}[\-.](02)[\-.](0[1-9]|1[0-9]|2[0-8])|(((18|19|20)(04|08|[2468][048]|[13579][26]))|2000)[\-.](02)[\-.]29)$/;
        isValid = pattern.test(value);
    }
    return isValid;
};
